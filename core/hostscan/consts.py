#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : consts.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 22:02
    *  @Software : PyCharm
    ***********************************
"""

CONSTS = {
    'config_status': {
        'INIT': 0,
        'WAIT': 1,
        'RUNNING': 2,
        'END': 3,
        'EXCEPTION': 4,
        'KILL': 5,
    },
    'task_status': {
        'INIT': 0,
        'RUNNING': 1,
        'END': 2,
        'EXCEPTION': 3,
        'KILL': 5,
    },
    'progress': {
        'INIT': 0,
        'RUNNING': 1,
        'END': 2,
        'EXCEPTION': 3,
    },
    'config_path': '/tmp',
    'speed': {
        'fast': '4',
        'normal': '3',
        'slow': '2',
        'fire_wall': '1',
    },
}