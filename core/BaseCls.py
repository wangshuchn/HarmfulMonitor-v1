#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : BaseCls.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 0:23
    *  @Software : PyCharm
    ***********************************
"""

import tornado.web
import tornado.ioloop
from core.utils.UtilCls import SessionManage

class BaseHandlers(tornado.web.RequestHandler):
    """
        Request基类
    """
    def initialize(self):
        self.session = SessionManage(self)

    @property
    def db(self):
        return self.application.session

    @property
    def cache(self):
        return self.application.cache

    def get_current_user(self):
        return self.get_secure_cookie("user")