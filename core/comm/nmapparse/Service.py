#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : Service.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 21:31
    *  @Software : PyCharm
    ***********************************
"""
import sys
import xml.dom.minidom


class Service:
    def __init__(self, ServiceNode):
        self.extrainfo = ServiceNode.getAttribute('extrainfo')
        self.name = ServiceNode.getAttribute('name')
        self.product = ServiceNode.getAttribute('product')
        self.fingerprint = ServiceNode.getAttribute('servicefp')
        self.version = ServiceNode.getAttribute('version')
        self.ostype = ServiceNode.getAttribute('ostype')


if __name__ == '__main__':

    dom = xml.dom.minidom.parse('i.xml')

    service_nodes = dom.getElementsByTagName('service')
    if len(service_nodes) == 0:
        sys.exit()

    node = dom.getElementsByTagName('service')[0]

    s = Service(node)