#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : Session.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 21:35
    *  @Software : PyCharm
    ***********************************
"""
import sys
import xml.dom.minidom


class Session:
    def __init__(self, SessionHT):
        self.start_time = SessionHT.get('start_time', '')
        self.finish_time = SessionHT.get('finish_time', '')
        self.nmap_version = SessionHT.get('nmap_version', '')
        self.scan_args = SessionHT.get('scan_args', '')
        self.total_hosts = SessionHT.get('total_hosts', '')
        self.up_hosts = SessionHT.get('up_hosts', '')
        self.down_hosts = SessionHT.get('down_hosts', '')


if __name__ == '__main__':
    dom = xml.dom.minidom.parse('i.xml')
    dom.getElementsByTagName('finished')[0].getAttribute('timestr')

    MySession = {'finish_time': dom.getElementsByTagName('finished')[0].getAttribute('timestr'), 'nmap_version': '4.79',
                 'scan_args': '-sS -sV -A -T4',
                 'start_time': dom.getElementsByTagName('nmaprun')[0].getAttribute('startstr'), 'total_hosts': '1',
                 'up_hosts': '1', 'down_hosts': '0'}

    s = Session(MySession)