#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : timezone.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 10:23
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from datetime import datetime as dtime

from conf.config import USE_TZ

ZERO = datetime.timedelta(0)
HOUR = datetime.timedelta(hours=1)

# print(ZERO)
# print(HOUR)

def now():
    """
    Return an aware or naive datetime.datetime, depending on config.USE_TZ
    """
    if USE_TZ:
        return dtime.utcnow()
    else:
        return dtime.now()