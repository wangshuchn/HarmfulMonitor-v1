#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : urltrim.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 21:40
    *  @Software : PyCharm
    ***********************************
"""

from urllib.parse import urlparse

def getDomain(url):

    """
    获取指定url的域名
    """
    url = url.strip().lower()
    if not url.startswith('http'):
        url = 'http://%s' % url
    info = urlparse(url)
    host = info.netloc.split(':')[0] # in case: ip:port
    return host

def getFormatUrl(url):
    """
    格式化URL
    """
    if not url.startswith(('http://', 'https://')):
        url = 'http://%s' % url
    if url.endswith('/'):
        url = url[:-1]
    return url