#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : JsonResponse.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/20 15:51
    *  @Software : PyCharm
    ***********************************
    定制公共消息类
"""
import json as serialize

class Http404(Exception):
    pass

class HttpResponseServerError():
    status_code = 500

class HttpResponseNotFound():
    status_code = 404

class HttpResponseForbidden():
    status_code = 403

class HttpResponseRedirect():
    status_code = 302


class Jsonify(object):
    """
    定制Layui Table返回格式
    """
    __slots__ = ("code", "msg", "count", "data")

    def __init__(self, code=0, msg="", count="", data=None):
        self.code = code
        self.msg = msg
        self.count = count
        self.data = data

    def json(self):
        # if not isinstance(self.data, dict):
        #     raise TypeError('Allow dict objects to be dumps')
        return serialize.dumps({
            "code": self.code,
            "msg": self.msg,
            "count": self.count,
            "data": self.data
        })

class JsonForm(object):
    """
    定制Layui Form返回格式
    """
    __slots__ = ("status", "msg")

    def __init__(self, status=0, msg=""):
        self.status = status
        self.msg = msg

    @property
    def Dict(self, *args, **kwargs):
        return {
            "status": self.status,
            "msg": self.msg
        }

if __name__ == '__main__':
    data = JsonForm(status='success', msg='增加成功').Dict
    print(data)