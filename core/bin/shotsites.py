#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : shotsites.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 20:44
    *  @Software : PyCharm
    ***********************************
    多进程对 hfmis_global_site 截图
"""

from multiprocessing import Pool
import os, sys, pymongo, datetime


# None alias for cpu count
POOL_SIZE = None

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
CMD_SHOOT = '{0}/phantomjs --ignore-ssl-errors=true {1}/screenshot.js'.format(
    SCRIPT_DIR, SCRIPT_DIR)
OUTPUT_DIR = '{0}/../ui/static/screenshot'.format(SCRIPT_DIR)
PID_FILE = '/var/tmp/shoot.pid'
NOW = datetime.datetime.now

def log(msg):
    """
    输出到控制台的内容
    """
    print('[%s] %s' % (NOW().strftime('%Y/%m/%d %H:%M:%S'), msg))

def shoot(item):
    """
    截图进程
    """
    os.system('%s %s %s/%s.png' % (CMD_SHOOT, item['site'], OUTPUT_DIR, item['_id']))

def dispatch():
    """
    调度：循环检测新任务
    """
    spec = {'deleted': False}
    p = Pool(POOL_SIZE, maxtasksperchild=40)
    p.imap(shoot, table.find(spec, {'site': 1}))
    p.close()
    p.join()

def ensure_single_instance():
    """
    同时只允许运行一个
    """
    if os.path.isfile(PID_FILE):
        old_pid = open(PID_FILE).read().strip()
        if os.system('ps -p %s >/dev/null' % old_pid) == 0:
            log('Already running, exiting')
            sys.exit(1)
    open(PID_FILE, 'w+').write(str(os.getpid()))

if __name__ == '__main__':
    import pymongo
    ensure_single_instance()

    log('Connecting database...')
    db = pymongo.Connection('localhost', 27017).sechfm
    table = db.websoc_global_site

    log('Start dispatching...')
    dispatch()
    log('Done')
