/**
 * @file: screenshot.js
 * @Author: wshu
 * @Time: 2020-04-16 21:00
 * @Fun: 截图取证
 * **/

var args = require('system').args
  , page = require('webpage').create()
  , timeout = 20000; // 20s后强制截图

if (args.length < 3) {
  console.error('Usage: phantomjs ', args[0], '<url> <save_as>');
  phantom.exit(1);
}

var url = args[1]
  , saveAs = args[2];

if (!/^https?:\/\//i.test(url)) {
  url = 'http://' + url;
}

page.clipRect = {
  left: 0,
  top: 0,
  width: 1024,
  height: 768
};
page.viewportSize = {
  width: 1024,
  height: 768
};

setTimeout(function () {
  console.error('Timeout exceed, force rendering:', url);
  page.render(saveAs);
  phantom.exit();
}, timeout);

console.log('Capturing', url, '...');
page.open(url, function (status) {
  if (status === 'fail') {
    console.error('Urlopen failed:', url);
    phantom.exit(2);
  }

  setTimeout(function () {
    page.render(saveAs);
    phantom.exit();
  }, 500);
});