#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : rebuild_screenshots.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 21:03
    *  @Software : PyCharm
    ***********************************
"""

import redis, json
from hashlib import md5

from conf.config import (redis_ip, redis_port, redis_auth)
# from Ui.utils import db

rd = redis.Redis(host=redis_ip, port=redis_port, password=redis_auth)
# sites = db.db.global_site.find(fields=['url', 'url_md5'])
#
# for site in sites:
#     rd.publish('ss:url', json.dumps({
#         'url': site['url'],
#         'save_as': 'site/' + site['url_md5'] + '.png',
#     }))
