#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : state.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 14:29
    *  @Software : PyCharm
    ***********************************
    任务组/ 任务/ 站点 /状态定义
"""

# group/task 在等待调度
ST_INIT = 0

# group/task/site 正在被调度
ST_SCHD = 1

# group/task/site 正在检测中
ST_SCAN = 2

# group/task/site 正常检测结束
ST_DONE = 3

# task 正在停止中
ST_STOP = 4

# task 等待恢复中
ST_RESUME = 5

# task/site 被人工停止
ST_DONE_STOP = 6

# task/site 被自动停止
ST_DONE_KILL = 7

# task/site 调度重启
ST_DONE_RESET = 8

# group 立即检测
ST_START_NOW = 9
