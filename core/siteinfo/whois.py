#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : whois.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 22:24
    *  @Software : PyCharm
    ***********************************
"""
import sys
import socket
from urllib.request import Request, urlopen
import re


def get_whois(site):
    """获取网站whois信息
       wshu 修改 2020-04-16
    """
    PORT = 43
    DomainSearch = {'com': 'whois.internic.net',
                    'net': 'whois.internic.net',
                    'org': 'whois.pir.org',
                    'nfo': 'whois.afilias.info',
                    'biz': 'whois.biz',
                    '.cc': 'whois.nic.cc',
                    'edu': 'whois.educause.net',
                    'mil': 'whois.nic.mil',
                    'gov': 'whois.nic.gov',
                    '.uk': 'whois.nic.uk',
                    '.us': 'whois.nic.us',
                    'ame': 'whois.nic.name',
                    'eum': 'whois.museum',
                    '.su': 'whois.ripn.net',
                    '.ru': 'whois.nic.ru',
                    'int': 'whois.iana.org',
                    '.ws': 'whois.worldsite.ws',
                    '.kr': 'whois.krnic.net',
                    '.jp': 'whois.nic.ad.jp',
                    '.it': 'whois.nic.it',
                    '.de': 'whois.denic.de',
                    '.fr': 'whois.nic.fr',
                    '.ca': 'whois.cira.ca',
                    '.cn': 'whois.cnnic.net.cn',
                    '.tw': 'whois.twnic.net.tw',
                    '.hk': 'whois.hkdnr.net.hk',
                    '.au': 'whois.aunic.net',
                    '.ac': 'whois.nic.ac',
                    'DEF': 'grs.hichina.com'}
    whoisDict = {'organization': '', 'email': '', 'expires': ''}

    if site.startswith('www.'):
        site = site[4:]
    domain = site[-3:]
    if not DomainSearch.get(domain):
        whoisserver = DomainSearch['DEF']
    else:
        whoisserver = DomainSearch[domain]
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((whoisserver, PORT))
        s.sendall((site + "\r\n").encode('utf-8'))
        response = ''

        while True:
            data = s.recv(4096).decode('utf-8')
            response += data
            if data == '':
                break
        s.close()
        response = response.lower()
        if response.find('no matching record.\n') > -1:
            whoisDict['expires'] = ''
            whoisDict['organization'] = ''
            whoisDict['email'] = ''
        else:
            try:
                pos = response.find('expiration time: ')
                pos_r = response.find('registrant: ')
                pos_e = response.find('registrant contact email: ')
                whoisDict['expires'] = response[pos + 17:pos + 28].strip()
                whoisDict['organization'] = response[pos_r + 12:response.index('registrant contact email: ')].strip()
                whoisDict['email'] = response[pos_e + 26:response.index('sponsoring registrar: ')].strip()
            except:
                whoisDict['expires'] = 'unknown'
                whoisDict['organization'] = 'unknown'
                whoisDict['email'] = 'unknown'
    except Exception as e:
        print(str(e))
        whoisDict['expires'] = 'time out'

    return whoisDict


def get_expires(site):
    """
    抓取 http://tool.chinaz.com/DomainDel/request.aspx?wd= 的域名到期信息
    """
    FETCH_URL = 'http://tool.chinaz.com/DomainDel/?wd='
    if site.startswith('www.'):
        site = site[4:]
    try:
        URL = FETCH_URL + site
        print(URL)
        req = Request(FETCH_URL + site)
        response = urlopen(req)
        html = response.read().decode('utf8')
        html = html.lower()
        p = r"""<div\sclass="fl\szTContleft">域名到期时间</div><div\sclass="fr\szTContrig"><span>([\d]{4}\-[\d]{2}\-[\d]{1,2})</span></div>"""
        expirdate = re.findall(p, html)
        expires = expirdate
    except Exception as e:
        expires = None

    return expires


if __name__ == '__main__':
    socket.setdefaulttimeout(8)
    print(get_whois("king321.com"))
    # try:
    #     site = sys.argv[1]
    # except:
    #     print('Usage: python whois.py yoursite.com')
    #     sys.exit(0)
    # print(get_expires(site))
