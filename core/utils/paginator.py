#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : paginator.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/19 16:30
    *  @Software : PyCharm
    ***********************************
"""
import collections.abc
import inspect
import warnings
from math import ceil

class RemovedInTornado6Warning(PendingDeprecationWarning):
    pass

class UnorderedObjectListWarning(RuntimeWarning):
    pass

class InvalidPage(Exception):
    pass


class PageNotAnInteger(InvalidPage):
    pass


class EmptyPage(InvalidPage):
    pass

def validate_number(number, per_page):
    """
    :return:
    """
    try:
        if isinstance(number, float) and not number.is_integer():
            raise ValueError
        if isinstance(per_page, float) and not number.is_integer():
            raise ValueError
        number = int(number)
        per_page = int(per_page)
    except (TypeError, ValueError):
        raise PageNotAnInteger('That page number is not an integer')
    if number < 1:
        raise EmptyPage('That page number is less than 1')
    elif per_page <1:
        raise EmptyPage('That page number is less than 1')
    return number, per_page