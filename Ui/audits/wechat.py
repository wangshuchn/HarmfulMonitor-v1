#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : wechat.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 12:50
    *  @Software : PyCharm
    ***********************************
"""
from core import BaseHandlers

class WechatViewHandler(BaseHandlers):
    """
    公众号审查管理
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('audit/wechat_list.html', error_msg=self.error_msg)


class WechatListHandler(BaseHandlers):
    """
    公众号审查列表
    """
    error_msg = ''

    def post(self, *args, **kwargs):
        self.write('ok')