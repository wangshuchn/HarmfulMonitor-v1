#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : microblog.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 12:51
    *  @Software : PyCharm
    ***********************************
"""
from core import BaseHandlers

class MicroBlogViewHandler(BaseHandlers):
    """
    微博审查管理
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('audit/microblog_list.html', error_msg=self.error_msg)


class MicroBlogListHandler(BaseHandlers):
    """
    微博审查列表
    """
    error_msg = ''

    def post(self, *args, **kwargs):
        self.write('ok')