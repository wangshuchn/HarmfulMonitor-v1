#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : wechat.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 12:23
    *  @Software : PyCharm
    ***********************************
"""
from core import BaseHandlers


class WechatViewHandler(BaseHandlers):
    """
    微信公众号任务视图
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('tasks/wechat_list.html', error_msg=self.error_msg)

class WechatListHandler(BaseHandlers):
    """
    微信公众号任务列表
    """
    error_msg = ''

    def post(self, *args, **kwargs):
        self.write('ok')
