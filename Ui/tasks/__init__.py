#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : __init__.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/8 22:29
    *  @Software : PyCharm
    ***********************************
"""

from Ui.tasks.task import (TaskViewHandler, TaskListHandler,
                           TaskAddHandler, TaskDeleteHandler)
from Ui.tasks.wechat import (WechatListHandler, WechatViewHandler)
from Ui.tasks.microblog import (MicroBlogListHandler, MicroBlogViewHandler)


__all__ = [TaskViewHandler, TaskListHandler, TaskAddHandler,
           WechatListHandler, WechatViewHandler,
           MicroBlogListHandler, MicroBlogViewHandler]