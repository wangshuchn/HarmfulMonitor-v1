#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : __init__.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 12:12
    *  @Software : PyCharm
    ***********************************
"""
from Ui.assets.assettask import (AssetsViewHandler, AssetsAddHandler)
from Ui.assets.assetlist import (AssetsListHandler, AssetsVulnScanHandler)

__all__ = [AssetsViewHandler, AssetsAddHandler,
           AssetsListHandler, AssetsVulnScanHandler]