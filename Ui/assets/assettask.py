#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : assettask.py
    *  @Author : wshu
    *  @CodeDate : 2020/5/28 10:49
    *  @Software : PyCharm
    ***********************************
    资产任务
"""
import time
import tornado.escape
from datetime import datetime, timedelta
from core import BaseHandlers
from core.hostscan.utils import get_ip_for_host
from core.comm.utils.timezone import now as timezone
from sqlalchemy import func, and_, or_, desc, cast, Numeric, DATE, distinct, extract
from core.comm.http.JsonResponse import Jsonify, JsonForm
from Ui.HFmSetting.utils.functional import paging
from Ui.HFmSetting.utils.checktool import check_task_name


class AssetsViewHandler(BaseHandlers):
    """
    资产任务管理视图
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('assets/asset_task.html', error_msg=self.error_msg)

    def post(self, *args, **kwargs):
        pass

class AssetsAddHandler(BaseHandlers):
    """
    资产任务添加
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('assets/asset_add.html', error_msg=self.error_msg)

    def post(self, *args, **kwargs):
        pass
