#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : __init__.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/8 22:31
    *  @Software : PyCharm
    ***********************************
"""

from Ui.charts.chart import (ChartViewHandler)

__all__ = [ChartViewHandler]