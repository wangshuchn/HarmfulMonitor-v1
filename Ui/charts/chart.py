#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : chart.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 11:58
    *  @Software : PyCharm
    ***********************************
"""
from core import BaseHandlers


class ChartViewHandler(BaseHandlers):
    """
    基础报表
    """
    error_msg = ''

    def get(self, *args, **kwargs):
        self.render('chart/chartview.html', error_msg=self.error_msg)