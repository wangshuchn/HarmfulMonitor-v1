#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : hfbase.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/8 22:46
    *  @Software : PyCharm
    ***********************************
"""
import os
import uuid
import base64
from conf.config import LOGIN_URL
DEBUG = True

# 定位路径
path = lambda root, *args: os.path.join(root, *args)
ROOT = os.path.abspath(os.path.dirname(__file__)).split('Ui')[0]
# COOKIE加盐
_saltstr = base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes)
hfconf = {
    'template_path': path(ROOT, "templates"),
    'static_path': path(ROOT, "static"),
    "cookie_secret": _saltstr,  # cookie自定义字符串加盐
    "login_url": LOGIN_URL,
    'xsrf_cookies': False,     # 防止跨站伪造
}