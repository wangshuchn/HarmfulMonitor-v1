#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : functional.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/19 16:42
    *  @Software : PyCharm
    ***********************************
"""
from core.utils.paginator import validate_number, EmptyPage, PageNotAnInteger



def paging(number, per_page):
    """
    分页
    Return a Page object for the given 1-based page number
    """

    number, per_page = validate_number(number, per_page)
    bottom = (number - 1) * per_page
    top = bottom + per_page
    return bottom, top


if __name__ == '__main__':
    page = 1
    limit = 20
    p, r = paging(page, limit)
    print(p, r)
    print(type(p), type(r))