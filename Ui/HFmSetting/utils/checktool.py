#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : checktool.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/21 16:14
    *  @Software : PyCharm
    ***********************************
"""
import re

def checkip(ip):
    p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
    if p.match(ip):
        return True
    else:
        return False

def checkpsd(passwd):
    p = re.match(r'^(?=.*?\d)(?=.*?[a-zA-Z]).{6,}$', passwd)
    if p:
        return True
    else:
        return False

def checkemail(email):
    p = re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email)
    if p:
        return True
    else:
        return False

def check_username(uname):
    p = re.match(r'^(?!\d+$)[\da-zA-Z_]+$', uname)
    if p:
        return True
    else:
        return False


def check_task_name(tname):
    """
    检查任务名称是否合法
    """
    p = re.match(r'^(?!_)[_a-zA-Z0-9\u4e00-\u9fa5]+$', tname)
    if len(tname) > 15:
        return False
    if p:
        return True
    else:
        return False

def check_strategy_name(strategy):
    """
    :param strategy: 检查策略检查是否合法
    :return:
    """