#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : config.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/8 23:43
    *  @Software : PyCharm
    ***********************************
    HarmfulMonitor 的全局配置文件
    Explain: 配置都大写，更明确是配置和常量，键大写，值小写
"""

import os
from urllib.parse import quote_plus

PRODUCT = 'HarmfulMonitor'

# 超级管理员
SUPER_USER = 'admin'
SUPER_USER_PASSWORD = 'sadew@233###'

# 设置登录初始路径
LOGIN_URL = '/signin'

# 核心目录
def get_env(env_key, default_value):
	env_value = os.environ.get(env_key)
	return env_value if env_value else default_value
SRC_PATH = get_env('SRC_PATH', '/opt/')
TMP_PATH = get_env('TMP_PATH', '/tmp/')
DATA_PATH = get_env('DATA_PATH', '/var/data/')
LOG_PATH = os.path.join(DATA_PATH, 'logs/')

# 基本配置
PRODUCT_HOME = os.path.join(SRC_PATH, PRODUCT)
APP_HOME = os.path.dirname(__import__('Ui').__file__)
REPORT_TOOLS_HOME = os.path.join(SRC_PATH, 'report-tools')

LICENSE_PATH = os.path.join(PRODUCT_HOME, 'LICENSE')
LICENSE_SECTION_NAME = 'LICENSE'

WEBVUL_DB_PATH = os.path.join(APP_HOME, 'plugins', 'webvul', 'vul.db')

# 【Memcache】
MEM_IP = '127.0.0.1'
MEM_PORT = '11211'

# 【Redis】
redis_ip = 'localhost'
redis_port = 6379
redis_auth = None
prefix = 'hfmis'

# 【rabbitmq】
MQURI = 'amqp://username:password@127.0.0.1/rabbitmq'

# 截图取证配置
sg_server = ''
evidence_path = os.path.join(DATA_PATH, 'evidence')

# GeoIP文件路径
GEOIP_PATH = os.path.join(DATA_PATH, 'geoip')
GEOIP_CITY_PATH = os.path.join(GEOIP_PATH, 'GeoIPCity.dat')
CN_CITY_PATH = os.path.join(GEOIP_PATH, 'cities.sqlite.dat')

# 远程RTCP端口转发服务器配置
RTCP_SERVER = ''

# 【MongoDB】
MONGO_HOST = '127.0.0.1'
MONGO_URI = 'mongodb://%s' % MONGO_HOST
MONGO_DB = 'sechfm'
MONGO_AUTO_START_REQUEST = True
MONGO_MAX_POOL_SIZE = 50

# 集群类型，主服务器ip, 自己ip
cluster_type = 'single'  # 可选值 'master', 'slave', 'single'
master_ip = '127.0.0.1'
node_ip = '127.0.0.1'

# 数据库服务器地址：必须放最后local_config会更新MYSQL_HOST
MYSQL_HOST = '192.168.2.128'
MYSQL_PORT = 3306
MYSQL_USERNAME = 'root'
MYSQL_PASSWORD = '123456'
MYSQL_DATABASE = 'sechfm'

BASH_PATH = os.path.dirname(__file__)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

UTIL_CONFIG = {
    'db_log_file' : BASE_DIR+'/logs/db.log',
    'web_log_file' : BASE_DIR+'/logs/web.log',
}
BASEUI = 'Ui\\'     # UI模块目录
# --使用连接池时需要此配置项
db = None
setting = {
    'manage': dict(db=db)
}
# 【mysql】
unitymob = "mysql+pymysql://{uname}:{paswd}@{host}:{port}/{db}?charset=UTF8MB4".\
    format(uname=MYSQL_USERNAME, paswd=quote_plus(MYSQL_PASSWORD), host=MYSQL_HOST, port=MYSQL_PORT, db=MYSQL_DATABASE)
# 【sqlalchemy】
echo = False
autoflush = True
pool_size = 10
pool_recycle = 7200

# Internationalization
USE_TZ = False