#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : local_config.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 12:28
    *  @Software : PyCharm
    ***********************************
    定制本地配置
"""
from conf.nodeconf import CNodeConf

# TODO 如果自定义数据目录，这里需要指定配置文件
nc = CNodeConf()

cluster_type = nc.setdefault('cluster_type', 'single')

if cluster_type == 'slave':
	master_ip = nc['master_ip']
	nc.setdefault('redis_ip', master_ip)
	nc.setdefault('MEM_IP', master_ip)
	nc.setdefault('MYSQL_HOST', master_ip)
	nc.setdefault('MONGO_URI', 'mongodb://%s' % master_ip)
	nc.setdefault('node_type', 'public')
else:
	nc.setdefault('master_ip', '127.0.0.1')

locals().update(nc)

__all__ = nc.keys()