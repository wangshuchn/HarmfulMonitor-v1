#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : urls.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:13
    *  @Software : PyCharm
    ***********************************
    this router used in app
    '''
        所有路由最后会集中统一供Server调度管理
        提供支持对Vue开放接口
        packages = get_categories(BASEUI)
        module_paths = get_files(packages, BASEUI)
        urls, spaApi = filter_mod(module_paths)
        # print(urls)
        # print(spaApi)
        # -- other route config explain:
        # urls.append((r'.*()', FileHandler, {"path": os.path.join(current_path, "public"), "default_filename": "index.html"}))
        # spa api-> spaApi
    '''
"""

from Route import (dash, users, tasks, strategy, audit, chart, assets)

urls_pattern = (
    # version: 1.0
    # 此方式应用于目前情况，总Route分发，统一进行管理，便于后期扩展
        dash.urls +
        users.urls +
        tasks.urls +
        audit.urls +
        strategy.urls +
        assets.urls +
        chart.urls
)