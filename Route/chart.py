#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : charts.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:13
    *  @Software : PyCharm
    ***********************************
"""

from Ui.charts import ChartViewHandler

urls = [
    (r"/charts", ChartViewHandler),
]