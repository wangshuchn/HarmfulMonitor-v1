#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : users.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:14
    *  @Software : PyCharm
    ***********************************
"""
from tornado.web import url
from Ui.users.user import (UserViewHandler, UserListHandler, UserInfoHandler, ChangePwdHandler,
                           AddUserHandler, DisableUserHandler, EnableUserHandler)

urls = [
    (r"/userview", UserViewHandler),   # 用户视图
    (r"/user/info", UserInfoHandler),     # 用户信息
    # (r"/user/changeinfo", ChangeInfoHandler),  # 修改信息
    (r"/user/list", UserListHandler),  # 用户列表
    (r"/user/add", AddUserHandler),  # 添加用户
    (r"/user/changepwd", ChangePwdHandler),  # 修改密码
    (r"/user/disable", DisableUserHandler),  # 停用
    (r"/user/enable", EnableUserHandler),  # 启用

    # (r"system/log", EnableHandler),  # 日志
    # (r"system/proxis", EnableHandler),  # 代理池

    # (r"manage/user/role", EnableHandler),  # 角色
    # (r"manage/user/backd", EnableHandler),  # 后台用户
]