#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : tasks.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:14
    *  @Software : PyCharm
    ***********************************
"""
from tornado.web import url
from Ui.tasks import (TaskAddHandler, TaskViewHandler, TaskListHandler, TaskDeleteHandler,
                      WechatViewHandler, WechatListHandler,
                      MicroBlogViewHandler, MicroBlogListHandler)

urls = [
    (r"/taskview", TaskViewHandler),    # 站点监测
    (r"/task/list", TaskListHandler),   # 任务列表
    (r"/task/add", TaskAddHandler),     # 任务添加
    (r"/task/delete", TaskDeleteHandler),     # 任务删除

    (r"/task/wx", WechatViewHandler),  # 微信公众号监测
    (r"/task/wx/list", WechatListHandler),  # 公众号列表

    (r"/task/wb", MicroBlogViewHandler),  # 微博监测
    (r"/task/wb/list", MicroBlogListHandler),  # 微博列表
]