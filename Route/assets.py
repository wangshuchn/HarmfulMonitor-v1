#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : assets.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/22 12:11
    *  @Software : PyCharm
    ***********************************
"""
from tornado.web import url
from Ui.assets import (AssetsViewHandler, AssetsAddHandler,
                       AssetsListHandler, AssetsVulnScanHandler)

urls = [
    (r"/assetview", AssetsViewHandler),         # 资产任务
    (r"/asset/list", AssetsListHandler),        # 资产列表
    (r"/asset/add", AssetsAddHandler),          # 添加资产
    (r"/asset/vulnscan", AssetsVulnScanHandler),# 资产扫描
    # (r"/asset/delete", AssetsViewHandler),    # 添加资产
]