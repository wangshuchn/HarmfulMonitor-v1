#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : dash.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/8 22:35
    *  @Software : PyCharm
    ***********************************
"""
from tornado.web import url
from Ui.dashboard import (MainHandler, IndexHandler, LogoutHandler, SignInHandler, DashboardHandler,
                          ArticleViewHandler, ArticleUpdateHandler)

urls = [
    (r"/", MainHandler),
    (r"/index", IndexHandler),        # 主页
    (r"/signin", SignInHandler),      # 登录
    url(r"/logout", LogoutHandler, name='logout'),      # 登出
    (r"/dashboard", DashboardHandler),      # 仪表盘
    # 知识共享
    (r"/article", ArticleViewHandler),  # 知识共享
    (r"/article/update", ArticleUpdateHandler),  # 知识库更新
]