#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : audits.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:13
    *  @Software : PyCharm
    ***********************************
"""
from Ui.audits import (AuditViewHandler, AuditListHandler, NumListHandler, AuditInfoHandler,
                       WechatViewHandler, WechatListHandler,
                       MicroBlogViewHandler, MicroBlogListHandler)

urls = [

    (r"/auditview", AuditViewHandler),    # 站点审查管理
    (r"/audit/list", AuditListHandler),    # 审查列表
    (r"/audit/num/(?P<site_id>\w+)", NumListHandler),    # 站点违规数量列表详情
    (r"/audit/info/(.*)", AuditInfoHandler),    # 审查信息

    (r"/audit/distort", AuditViewHandler),  # 篡改审查管理
    (r"/audit/distort/list", AuditListHandler),  # 篡改审审查列表

    (r"/audit/wx", WechatViewHandler),    # 微信公众号审查管理
    (r"/audit/wx/list", WechatListHandler),    # 微博审查列表

    (r"/audit/wb", MicroBlogViewHandler),  # 微博审查管理
    (r"/audit/wb/list", MicroBlogListHandler),  # 微博审查列表
]