#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : strategy.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:14
    *  @Software : PyCharm
    ***********************************
"""
from tornado.web import url

from Ui.strategy import (StrategyAddHandler, StrategyViewHandler, StrategyDeleteHandler,
                         StrategyTextHandler, StrategyImageHandler,
                         StrategyKeywordListHandler, KeywordTableListHandler, KeywordAddHandler)

urls = [
    url(r"/strategyview", StrategyViewHandler),
    # url(r"/strategy_delete", StrategyDeleteHandler),

    (r"/keyword_strategy_add", StrategyAddHandler),
    (r"/keyword_strategy_list", StrategyKeywordListHandler),
    (r"/keyword/list/(?P<strategy_id>\w+)", KeywordTableListHandler),
    url(r"/keyword/add/(.*)", KeywordAddHandler, name='keywordadd'),

    (r"/text_strategy_add", StrategyTextHandler),
    (r"/image_strategy_add", StrategyImageHandler),
]