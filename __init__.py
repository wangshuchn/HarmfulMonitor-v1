#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : __init__.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/17 21:13
    *  @Software : PyCharm
    ***********************************
"""
SYSTEM = 'HarmfulMIS-v1'
VERSION = '1.0.0'
