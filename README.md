# HarmfulMonitor
====================

#### 介绍
内容安全监测平台，包含网站监测、微博监测、微信公众号监测、抖音、快手短视频监测以及漏洞管理、安全自动化扫描功。
敏感词策略定制，高效过滤色情、广告、涉政、暴恐等多类垃圾文字及敏感词、语音、视频、图片、等违规内容识别。有效管控业务违规风险。

#### Server 端必备环境

- 安装`MySQL`（version： 5.6+）

- 安装`Python`环境(version： 3.6+)

#### 软件架构
后端架构：Tornado6.0 + Mysql + Redis
前端展示: bootstrap + layui
任务调度：Celery
`follow-up: (TiDB)`


#### 安装教程

1.  安装依赖：`pip install -r requirements.txt`
2.  切换到项目里执行：`python3 manage.py` 或 `python3 manage.py --addr localhost -port 8888` (提供指定IP运行,默认localhost)

#### 使用说明

1.  切换到项目工程
2. 方式一: python manage.py 
2. 方式二: python3 manage.py --addr 127.0.0.1 --port 8888




