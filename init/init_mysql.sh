#!/usr/sh

SHELL_PATH=${PYTHONPATH}/HarmfulMonitor/init
# CREATE_DBS=$'CREATE DATABASE db_name DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;'

if [ ${1}x = 'surex' ];
then
	.  ${SHELL_PATH}/_include_pwd.sh
	mysql -uroot -p${ROOT_PWD} mysql < ${SHELL_PATH}/_createdb.sql
	sleep 1
	mysql -uiscanv -p${HFMONITOR_PWD} iscanv < ${SHELL_PATH}/_initdb.sql
	echo '>> ok !'
else
	echo '>> you are sure ?'
fi