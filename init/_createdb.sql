CREATE DATABASE IF NOT EXISTS  iscanv;
--  集群模式下 有BUG 导致子节点 无法连接  检查关键无法检测
use mysql;
delete from user where not(host="localhost");
use iscanv;
CREATE USER 'sechfm'@'localhost' IDENTIFIED BY 'sechfm@';
GRANT ALL PRIVILEGES ON 'sechfm'.'*' TO ''@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
