#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : siteinfo_event.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 16:21
    *  @Software : PyCharm
    ***********************************
"""
from db.mongo import Event

class SiteinfoEvent(Event):

	structure = {
		'value': {
			'title': str,
			'host': str,
			'ip': str,

			'longitude': float, 'latitude': float,  # 经度和纬度
			'city': str,
			'region': str,
			'country': str,
			'continent': str,

			'pr': int,
			'alexa': int,
			'whois': str,
			'icp': str,

			'server': str,
			'technology': str,
			'os': str,

			'http_method': str,
			'port': [
			],
		},
	}

	required_fields = []

	default_values = {}
