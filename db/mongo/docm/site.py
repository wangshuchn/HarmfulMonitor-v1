#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : site.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:49
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from bson.objectid import ObjectId
from db.mongo import Document

from mongokit.operators import IS

from db.mongo.docm import fixed

class Site(Document):

	__collection__ = 'hfm_site'

	structure = {
		# 说明: ID, 例如: 9527 索引: 默认
		# 用处:
		#   前台: 用于给程序区分不同站点
		#   后台: 用于给程序区分不同站点
		#   调度: 用于给程序区分不同站点
		#   统计: 用于给程序区分不同站点
		'_id': int,

		'v_group_id': ObjectId,

		# 说明: 任务ID, 例如: 9527
		# 索引: 可能需要, 会按task_id查询?
		# 用处:
		#   前台: 用于给程序区分该站点所属的任务
		#   后台: 用不到
		#   调度: 用于给程序区分该站点所属的任务
		#   统计: 用于给程序区分该站点所属的任务, 方便更新任务的统计数据
		'task_id': int,

		# 说明: 任务组ID, 例如: 9527
		# 索引: 可能需要, 会按group_id查询?
		# 用处:
		#   前台: 用于给程序区分该站点所属的任务组
		#   后台: 用不到
		#   调度: 用于给程序区分该站点所属的任务组
		#   统计: 用于给程序区分该站点所属的任务组, 方便更新任务组的统计数据
		'group_id': int,

		# 同hfm_group
		'user_id': int,
		'site': str,
		'module_type': str,  # New in version 4.2.0

		# 说明: 关联全局站点的引用
		# 用处:
		#   前台: 用于不同任务组下的站点整合
		#   后台: 用不到
		#   调度: 用不到
		#   统计: 用于不同任务组下的站点整合
		'g_site_id': ObjectId,

		# 说明: 调度状态
		'status': IS(
			fixed.ST_SCHD, fixed.ST_SCAN, fixed.ST_DONE,
			fixed.ST_DONE_STOP, fixed.ST_DONE_KILL, fixed.ST_DONE_RESET
		),

		'created_at': datetime.datetime,

		# 说明: 该站点检测开始时间
		# 用处:
		#   前台: 展示检测开始时间, 和end_at一起计算检测用时
		#   后台: 用不到
		#   调度: 记录开始时间
		#   统计: 用不到
		'start_at': datetime.datetime,

		# 说明: 该站点检测结束时间
		# 用处:
		#   前台: 展示检测结束时间, 和start_at一起计算检测用时
		#   后台: 用不到
		#   调度: 记录结束时间
		#   统计: 用于记录到当天的综合分析数据的时间判断依据
		'end_at': datetime.datetime,

		# 类型: string, 可能为None
		# 意义: 节点ID, 标识此站点在哪个爬虫节点上运行的
		'node_id': str,  # New in version 4.1.x????
	}

	required_fields = [
		'_id', 'v_group_id', 'task_id', 'group_id', 'user_id',
		'site', 'module_type', 'g_site_id', 'status',
		'created_at',
	]

	default_values = {
		'status': fixed.ST_SCHD,
		'created_at': datetime.datetime.now,
		'start_at': None, 'end_at': None,
		#'node_id': ''
	}

	indexes = [
		{'fields': [('g_site_id', 1)]},
		{'fields': [('v_group_id', 1)]},
		{'fields': [('group_id', 1)]},
		{'fields': [('task_id', 1)]},
		{'fields': [('user_id', 1)]},
		{'fields': [('created_at', 1)]},
		{'fields': [('start_at', -1)]},
		{'fields': [('end_at', -1)]},
		{'fields': [('site', 1)]},
		{'fields': [('status', 1)]},
		{'fields': [('module_type', 1)]},
	]