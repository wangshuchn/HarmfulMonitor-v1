#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : notice_config.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:58
    *  @Software : PyCharm
    ***********************************
"""

import datetime
from bson.objectid import ObjectId

from db.mongo import Document

class NoticeConf(Document):

	__collection__ = 'hfm_notice_conf'

	structure = {
			'user_id': int,

			# 用以获取v_group_name，message_type为stat时为None
			'v_group_id': ObjectId,

			# 判别notice模块，message_type为stat时为None
			'module_type' : str,

			# 已经发送的次数
			# 示例：0~7
			'times' : int,

			# 下次发送的时间
			'time_next' : datetime.datetime,

			# 信息主体 短信的subject为空
			'message': {
				'subject': str,
				'content': str,
				},

			# 发信目标
			# 模式： 18701015792 wangshuchn@163.com.com
			'send_to' : str,

			# 是否单次发送
			# 单次发送的信息配置会在发送完之后删除
			'once': bool,

			# 信息种类
			# 示例：'stat', 'detail'
			'message_type': str,

			# 发送方式
			# 示例：'sms', 'email'
			'send_type': str,

			# 抄送
			'cc': [str]
	}

	required_fields = ['send_to', 'time_next', 'once', 'message_type', 'send_type']

	default_values = {
			'v_group_id': None,
			'module_type': None,
			'time_next': datetime.datetime.now,
			'message': {'subject': None},
			'once': False,
			'message_type': 'detail',
			'cc': []
	}