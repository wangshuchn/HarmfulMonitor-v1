#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : notice.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 15:17
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from db.mongo import Document

class Notice(Document):
    __collection__ = 'hfm_notice'

    structure = {

        # 用户ID
        # 前后台：用于权限确认，用户数据分离等
        'user_id': int,

        'created_at': datetime.datetime,

        # 发送方式
        # 示例： 'sms', 'email'
        'send_type': str,

        # 发送到，手机或邮箱
        # 示例：18701015792 wangshuchn@163.com.com
        'send_to': str,

        # 信息种类
        # 示例： 'detail', 'stat'
        'message_type': str,

        # 信息本体
        'message': {
            'subject': str,  # 主题  邮件需要，短信为空且无效
            'content': str,  # 消息正文
        },

        # 报警的模块
        # 主要用于搜索分类
        # 统计信息则为None
        'module_type': str,

        # 任务组名
        # 用于搜索
        'v_group_name': str,

        # 抄送
        'cc': [str]
    }

    required_fields = [
        'user_id', 'created_at', 'send_type', 'send_to', 'message_type',
        'v_group_name', 'message'
    ]

    default_values = {
        'created_at': datetime.datetime.now,
        'message': {'subject': None},
        'cc': []
    }
