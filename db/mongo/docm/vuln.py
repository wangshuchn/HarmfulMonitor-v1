#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : vuln.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:49
    *  @Software : PyCharm
    ***********************************
"""
from mongokit.operators import IS

structure = {
    'value': {
        # 说明: 漏洞库中对应记录的ID
        # 示例: 255
        'id': int,

        # 说明: POC的检测结果
        # 示例:
        # {
        #     "Status": 1, "Result": {
        #         "DBInfo" : {
        #             "Username": "phpcms",
        #              "Password": "41b2c83b73a293ccee5b40dd1d47d804"
        #         }
        #     }
        # }
        'poc_result': {
            # 说明: POC状态
            # 0 - 无POC结果
            # 1 - 有POC结果
            'Status': IS(0, 1),

            # 说明: POC结果
            'Result': {
                'URL': str,  # POC检测识别的路径
                'AdminInfo': {
                    'Uid': str,
                    'Password': str,
                    'Username': str,
                },
                'Database': {
                    'Hostname': str,
                    'DBname': str,
                    'Username': str,
                    'Password': str,
                },
                'DBInfo': {
                    'Username': str,
                    'Password': str,
                    'Salt': str,
                    'Uid': str,
                    'Groupid': str,
                    'ShortName': str,
                    'Type': str,
                },
                'VerifyInfo': {
                    'URL': str,
                    'URL1': str,
                    'URL2': str,
                    'Path': str,
                    'Postdata': str,
                    'Payload': str,
                },
                'ShellInfo': {
                    'URL': str,
                    'Content': str,
                    'Cookie': str,
                },
                'SiteAttr': {
                    'Process': str,
                },
                'FileInfo': {
                    'Filename': str,
                    'Content': str,
                },
                'XSSInfo': {
                    'URL': str,
                    'Payload': str,
                },
            },
        },
    },
}

required_fields = ['value.id']

default_values = {
    'type': 'vuln',
    'module': 'hfmonitor',
}