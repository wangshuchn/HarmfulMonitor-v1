#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : fixed.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 14:27
    *  @Software : PyCharm
    ***********************************
"""

# 任务组/任务/站点 状态定义
# 代码: Ui.core.sched import state
ST_INIT = 0  # group/task 等待调度
ST_SCHD = 1  # group/task/site 正在被调度
ST_SCAN = 2  # group/task/site 正在检测中
ST_DONE = 3  # group/task/site 正常检测结束
ST_STOP = 4  # task 正在停止中
ST_RESUME = 5  # task 等待恢复中
ST_DONE_STOP = 6  # task/site 被人工停止
ST_DONE_KILL = 7  # task/site 被自动停止
ST_DONE_RESET = 8  # task/site 调度重启
ST_START_NOW = 9  # group 立即检测

PRIORITY_DEFAULT = 6


CONTENT_PLUGINS = ('black_links', 'malscan', 'keyword', 'deface')
DETECTION_PLUGINS = ('picture', 'texts', 'audio', 'video', 'webvul', 'info_leak', 'form_crack')
ALL_PLUGINS = CONTENT_PLUGINS + DETECTION_PLUGINS


INFO_LEAKS = [
	'ip_address',       # 内网IP地址泄露
	'dbms_info',        # 数据库信息泄露
	'debug_info',       # 服务端调试信息泄漏
	'auto_indexes',     # Web服务器目录浏览
	'web_path',         # Web服务器网站路径泄漏
	'email',            # 电子邮件地址泄漏
	'flash_object',     # Flash参数配置风险
	'source_code',      # 源代码泄露
	'apps_error_info',  # 服务器端应用程序错误
]