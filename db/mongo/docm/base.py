#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : base.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:48
    *  @Software : PyCharm
    ***********************************
"""
from conf import config
from mongokit.document import Document as MongoKitDocument
from db.mongo.docm.fixed import ALL_PLUGINS, INFO_LEAKS

def get_extend_stat():
	return dict([(i, {'total': int}) for i in INFO_LEAKS])

def get_plugin_stat(excludes=[]):
	stat_dict = {
		'site_id': int,
		'total': int, 'show': int, 'hidden': int,
		'high': int, 'medium': int, 'low': int, 'nought': int
	}

	for exclude in excludes:
		if exclude in stat_dict:
			del stat_dict[exclude]
	return dict([(i, stat_dict.copy()) for i in ALL_PLUGINS])


def unicode_and_str(value_list):
	unicode_and_str_list = []
	for value in value_list:
		unicode_and_str_list.append(str(value))
		unicode_and_str_list.append(str(value))
	return unicode_and_str_list


class Document(MongoKitDocument):

	use_dot_notation = True
	use_schemaless = True
	authorized_types = [
		str,
	]
	__database__ = config.MONGO_DB


