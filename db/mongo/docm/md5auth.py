#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : md5auth.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 17:33
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from db.mongo import Document

class Md5auth(Document):

	__collection__ = 'hfm_md5auth'

	structure = {

		# 说明：MD5字符串，用于识别是否有权限显示站点列表
		'list_md5': str,

		# 说明：包含字典的列表
		# 用处：用于保存此权限可显示的列表中包含的站点，以及连接到此站点报表所需要验证的MD5
		'report_md5': [{
			'g_site_id': str,
			'md5': str,
		}],

		# 说明：指明此MD5创建时间，用于判定此MD5是否在有效期之内
		# 非有效期内的MD5应该由清理函数清理
		'created_at': datetime.datetime,
	}

	required_fields = [
		'list_md5', 'report_md5', 'created_at',
	]