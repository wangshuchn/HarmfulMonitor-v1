#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : domain.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 16:13
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from db.mongo import Document

class Domain(Document):

	__collection__ = 'hfm_domain'

	structure = {
		'domain': str,
		'ip': str,
		'created_at': datetime.datetime,
		'update_at': datetime.datetime,
	}

	required_fields = ['domain', 'ip', 'created_at', 'update_at']

	default_values = {
		'created_at': datetime.datetime.now,
		'update_at': datetime.datetime.now,
	}