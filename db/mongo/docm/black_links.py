#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : black_links.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 14:01
    *  @Software : PyCharm
    ***********************************
"""
from db.mongo import Event

class BlackLinksEvent(Event):

	structure = {
		'value': {

			# 说明: 存在暗链的URL
			# 示例: "http://www.fuck.com/index.html"
			'url': str,

			# 说明: 暗链链接列表
			# 示例: ["http://www.api.com/index.html", "http://www.bpi.com/index.html", "http://www.cpi.com/index.html"]
			'links': [str],

			# 说明: 暗链词库列表
			# 示例: ["口罩", "要口罩", "我要口罩"]
			'keywords': [str],
		},
	}

	required_fields = ['value.url', 'value.links']

	default_values = {
		'type': 'black_links',
		'module': 'appdetect',
	}

	mapper = {
		'level': None,
	}