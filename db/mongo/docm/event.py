#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : event.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 14:02
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from bson.objectid import ObjectId
from db.mongo import Document


class Event(Document):

	__collection__ = 'hfm_event'

	structure = {
		'_id': int,
		'v_group_id': ObjectId,
		'site_id': int,
		'task_id': int,
		'group_id': int,
		'g_site_id': ObjectId,
		'created_at': datetime.datetime,
		'user_id': int,
		'module': str,
		'type': str,
		'value': {},
		'ignore': bool,  # 4.2.0
		'feedback': bool,
		'feature_id': ObjectId,  # 4.2.0
	}

	required_fields = [
		'_id', 'v_group_id', 'site_id', 'task_id', 'group_id', 'g_site_id',
		'created_at', 'user_id', 'module', 'type', 'ignore'
	]

	default_values = {
        'ignore': False,
        'created_at': datetime.datetime.now,
	}

	mapper = {
		'igore': 'ignore'
	}

