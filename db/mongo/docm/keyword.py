#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : keyword.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:59
    *  @Software : PyCharm
    ***********************************
"""
from mongokit.operators import IS

from db.mongo import Document

LEVEL = {
	'low': 1,
	'medium': 2,
	'high': 3,
}
TYPE = {
	'涉政': 1,
	'涉黄': 2,
	'涉赌': 3,
	'涉毒': 4,
	'涉暴': 5,
	'低俗': 6,
	'反腐': 7,
	'民生': 8,
}


class Keyword(Document):

	__collection__ = 'hfm_keyword'

	structure = {
		'word': str,
		'level': IS(*LEVEL.values()),
		'type': IS(*TYPE.values()),
		'enabled': bool,
	}

	required_fields = ['word', 'level', 'type', 'enabled']

	default_values = {'enabled': True}
