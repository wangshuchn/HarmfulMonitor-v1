#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : report_export_task.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 15:35
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from bson.objectid import ObjectId
from db.mongo import Document, unicode_and_str

from mongokit.operators import IS, OR


STATUS_DICT = {
	'new': 0,  # 新建，等待导出
	'exporting': 1,  # 进行，正在导出
	'done': 2,  # 完成，可以下载
}

class ReportExportTask(Document):

	__collection__ = 'hfm_report_export_task'

	STATUS = STATUS_DICT

	structure = {

		# 导出报表的任务名
		'export_name': str,

		# 导出报表类型
		'export_type': IS(*unicode_and_str([
			'pdf',      # PDF文件
			'doc',      # Word文件，实际上是RTF格式
		])),

		# 大多数情况需要用虚拟组进行区分展示
		'v_group_id': ObjectId,

		# 当类别为 task_(content|weakness)时需要区分 task
		'task_id': int,

		# 区分用户
		'user_id': int,

		# 前台进行弹出框区分的类别
		'export_class': IS(*unicode_and_str([
			'global_site',  # 站点监控弹出框
			'virtual_group',  # 任务（虚拟组） 列表中的弹出框
			'task_weakness',  # 时间片（漏洞）弹出框
			'task_content'  # 时间片（安全事件）弹出框
		])),

		# 依据什么条件导出
		'export_by': IS(*unicode_and_str([
			'g_site_id',  # 直接根据g_site_id清单导出
			'v_group_id',  # 根据v_group_id查询得到g_site_id清单，再根据这个清单导出
			'task_id',  # 根据task_id查询得到site_id清单，再根据这个清单导出
		])),

		# 对应export_by依据的ID清单
		'export_ids': [OR(int, ObjectId)],

		# 状态
		'status': IS(*STATUS_DICT.values()),

		# 创建时间
		'created_at': datetime.datetime,

		# 开始时间
		'start_at': datetime.datetime,

		# 结束时间
		'end_at': datetime.datetime,
	}

	required_fields = [
		'export_name', 'export_by', 'export_ids',
		'export_class', 'user_id',
		'status', 'created_at',
	]

	default_values = {
		'status': STATUS_DICT['new'],
		'created_at': datetime.datetime.now,
		'v_group_id': None,
		'task_id': None,
		'start_at': None,
		'end_at': None,
	}