#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : task.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 13:49
    *  @Software : PyCharm
    ***********************************
"""
import datetime
from bson.objectid import ObjectId

from mongokit.operators import IS

from db.mongo import Document
from db.mongo.docm import fixed

TASK_STATUS = {
	'INIT': 0,
	'SCHD': 1,
	'DETC': 2,
	'DONE': 3,
}


class Task(Document):

	__collection__ = 'hfm_task'

	STATUS = TASK_STATUS

	structure = {
		# 说明: ID, 例如: 9527
		# 索引: 默认包含索引
		# 用处:
		# 	前台: 用于给程序区分不同任务
		# 	后台: 用于给程序区分不同任务
		# 	调度: 用于给程序区分不同任务
		# 	统计: 用于给程序区分不同任务
		'_id': int,


		'v_group_id': ObjectId,

		# 说明: 任务组ID, 例如: 9527
		# 索引: 可能需要, 会按group_id查询?
		# 用处:
		# 	前台: 用于给程序获取该任务所属的任务组
		# 	调度: 用于给程序获取该任务所属的任务组
		# 	统计: 用于给程序获取该任务所属的任务组, 方便更新任务组的统计数据
		'group_id': int,

		'g_site_id_list': [ObjectId],

		'site_list': [str],

		# 说明: 调度状态
		'status': IS(
			fixed.ST_INIT, fixed.ST_SCHD, fixed.ST_SCAN, fixed.ST_DONE,
			fixed.ST_STOP, fixed.ST_RESUME,
			fixed.ST_DONE_STOP, fixed.ST_DONE_KILL, fixed.ST_DONE_RESET
		),

		'user_id': int,
		'module_type': str,
		'created_at': datetime.datetime,
		'start_at': datetime.datetime,
		'end_at': datetime.datetime,
		'level': int,
		'priority': int,  # 4.2.0
		'modules': {
			'siteinfo': bool,
			'availability': [str],
			'content': {
				'malscan': {'enabled': bool, 'mode': str, 'evidence': bool},
				'keyword': {'level': int, 'sys': bool, 'evidence': bool, 'usr_file': str},
				'wad': bool, 'black_links': bool,
			},
			'detection': {
				'picture': {'enabled': bool, 'place': [str], 'picture': bool, 'mode': str},
				'texts': {'enabled': bool, 'place': [str], 'texts': bool, 'mode': str},
				'audio': {'enabled': bool, 'place': [str], 'audio': bool, 'mode': str},
				'video': {'utf7': bool, 'enabled': bool, 'audio': bool, 'mode': str},
				'webvul': {'enabled': bool, 'webvul_verify': [int], 'attack': bool},
				'content_leak': {'enabled': bool, 'check_list': str},
			},
		},
		'spider': {
			'max_page': int, 'max_url_param': int, 'thread_count': int, 'delay': int, 'depth': int, 'timeout': int,
			'ua': str, 'referer': str, 'proxy': str,
			'crawl_scope': int, 'gather_scope': int,
			'exclude_domains': [str], 'include_domains': [str],
			'exclude_urls': [str], 'include_urls': [str],
			'auth': str,
			'pre_login': {
				'url': str,
				'params': str,
			},
		},
		'run_at': datetime.datetime,

		# 说明:
		# 用处:
		# 	前台: 展示基本的、常用的统计信息, 用于站点监控的展示
		# 	统计: 由分析统计模块写入数据
		'stat': {
			'level': int,  # 0 - 无；1 - 低；2 - 中； 3 - 高, 那次检测中风险等级最高的站点的等级
			'high': int,  # 风险等级为高的站点数量
			'medium': int,  # 风险等级为中的站点数量
			'low': int,  # 风险等级为低的站点数量
			'nought': int,  # 风险等级为无的站点数量
			'content_high_sites': int,
			'weakness_high_sites': int,
			'sum_sites': int,
			'content_high_events': int,
			'weakness_high_events': int,
			'modules': {
				'siteinfo': {
					'icp': int,  # ICP备案的站点数量
				},
				'availability': {
					'dead': int,  # 不存活的站点数量
					'hijacked': int,
				},
			}
		},
		'node_ids': {'0': int},
		'temp': bool,
	}

	required_fields = [
		'_id', 'v_group_id', 'group_id', 'g_site_id_list', 'site_list',
		'status', 'user_id', 'module_type', 'created_at', 'start_at', 'end_at',
		'level', 'priority', 'modules', 'spider', 'run_at', 'temp',
		'spider.delay', 'spider.depth', 'spider.max_page', 'spider.max_url_param', 'spider.timeout', 'spider.thread_count',
	]

	default_values = {
		'created_at': datetime.datetime.now,
		'status': fixed.ST_INIT, 'start_at': None, 'end_at': None, 'level': None,
		'priority': fixed.PRIORITY_DEFAULT, 'run_at': None,
		'stat': {},
		'temp': False,
		'spider.max_url_param': 10, 'spider.thread_count': 20, 'spider.delay': 0, 'spider.timeout': 30,
	}

	mapper = {
		'node_id': 'node_ids',
		'v_group_module_type': 'module_type',
	}