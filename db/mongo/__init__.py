#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : __init__.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:46
    *  @Software : PyCharm
    ***********************************
    后期计划采用mongo存储，设计了一部分集合结构
"""

from db.mongo.docm.base import Document, unicode_and_str
from db.mongo.docm.event import Event


__all__ = [
    Document, Event, unicode_and_str
]