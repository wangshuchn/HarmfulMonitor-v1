#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : ARTICLE.py
    *  @Author : wshu
    *  @CodeDate : 2020/6/14 10:04
    *  @Software : PyCharm
    ***********************************
    知识共享
"""
from . import BaseModel
from sqlalchemy.sql import expression
from sqlalchemy.orm import relationship
from sqlalchemy import (Column, ForeignKey, Table)
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime, LargeBinary)

from .RBAC import User
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone


ARTICLE_STATUS = (
    ('0', '新建'),
    ('1', '发布'),
    ('2', '撤回'),
    ('3', '审核'),
)

class ArticleType(BaseModel):
    """
    知识共享分类模型
    """
    __tablename__ = 'article_type'

    article_type_name = Column(String(30), nullable=True, comment='文章分类')
    article_type_body = Column(Text, nullable=True, comment='分类简介')
    parent = Column(Integer, ForeignKey('article_type.id', ondelete='CASCADE'))

    def __str__(self):
        return '---', self.article_type_name


class Article(BaseModel):
    """
    知识库
    """
    __tablename__ = 'article'

    article_id = Column(String(30), nullable=True, comment='文章ID')
    article_name = Column(String(50), nullable=True, unique=True, comment='文章标题')
    article_order = Column(Integer, nullable=True, default=0, comment='文章推广')
    file = Column(LargeBinary, comment='附件')
    article_body = Column(Text, comment='文章内容')
    article_status = Column(String(25), ChoiceType(ARTICLE_STATUS), default='0', comment='文章状态')
    create_at = Column(DateTime, default=timezone.now, comment='添加时间')
    update_at = Column(DateTime, default=timezone.now, comment='更新时间')

    article_type = Column(Integer, ForeignKey('article_type.id', ondelete='CASCADE'), comment='文章分类')
    article_user = Column(Integer, ForeignKey(f'{User.__tablename__}.id', ondelete='CASCADE'), comment='用户关联')

    def __str__(self):
        return self.article_id

#
ARTICLE_COMMENTS_STATUS = (
    ('0', '已评论'),
    ('1', '未评论'),
)
class ArticleComments(BaseModel):
    """
    文章评论
    """
    __tablename__ = 'article_comment'

    article_comment_id = Column(String(30), comment='评论ID')
    article_comment_body = Column(Text, comment='评论内容')
    article_comment_status = Column(String(25), ChoiceType(ARTICLE_COMMENTS_STATUS), default='0', comment='评论状态')
    start_at = Column(DateTime, default=timezone.now, comment='添加时间')

    article_comment_article = Column(Integer, ForeignKey('article.id', ondelete='CASCADE'), comment='文章关联')
    article_comment_user = Column(Integer, ForeignKey(f'{User.__tablename__}.id', ondelete='CASCADE'), comment='用户关联')