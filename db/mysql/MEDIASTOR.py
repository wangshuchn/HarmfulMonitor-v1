#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : MEDIASTOR.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 13:38
    *  @Software : PyCharm
    ***********************************
    爬虫/检测基础模型
"""
from . import BaseModel
from sqlalchemy import Column
from sqlalchemy.sql import expression
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import (String, Integer, FLOAT, Boolean, Text, DateTime)

from .TASK import Task, Site
from .TACTIC import Strategy
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone

DETECT_TYPE = (
    (u'0', u'未检测'),
    (u'1', u'已检测'),
    (u'2', u'检测中'),
)
class TextStor(BaseModel):
    """
    文本存储
    """
    __tablename__ = 'text_stor'

    text_url = Column(String(200), nullable=False, comment='链接地址')
    text_body = Column(Text, nullable=False, comment='正文内容')
    kwstatus = Column(String(25), ChoiceType(DETECT_TYPE), default='0', comment='关键词检测状态')
    aistatus = Column(String(25), ChoiceType(DETECT_TYPE), default='0', comment='智能检测状态')
    snapshot = Column(String(150), nullable=True, comment='快照')
    grab_time = Column(DateTime, default=timezone.now, comment='抓取时间')
    # 任务/站点-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')

    def __str__(self):
        return self.text_url

class ImageStor(BaseModel):
    """
    图片存储
    """
    __tablename__ = 'image_stor'

    imgurl = Column(String(200), nullable=False, comment='原链接地址')
    imgpath = Column(String(200), nullable=False, comment='存储本地路径')
    imgsize = Column(Integer, nullable=True, comment='图片大小')
    detect_status = Column(String(25), ChoiceType(DETECT_TYPE), default='0', comment='图片检测状态')
    grab_time = Column(DateTime, default=timezone.now, comment='抓取时间')
    # 任务/站点-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')

    def __str__(self):
        return self.imgurl


class BadurlStor(BaseModel):
    """
    坏链存储
    """
    __tablename__ = 'badurl_stor'

    url = Column(String(200), nullable=False, comment='链接地址')
    status = Column(String(25), nullable=False, comment='网站状态')
    detect_time = Column(DateTime, default=timezone.now, comment='检测时间')
    # 站点关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')

    def __str__(self):
        return self.url

# 结果类型
RESULT_TYPE = [
    (u'涉黄', u'涉黄'),
    (u'涉政', u'涉政'),
    (u'反腐', u'反腐'),
]
# 凭证类型
VOUCHER_TYPE = [
    (u'keyword', u'关键词'),
    (u'ai', u'智能识别'),
    (u'other', u'其他'),
]
class TextResultStor(BaseModel):
    """
    文本检测结果存储
    """
    __tablename__ = 'text_res'

    origin_addr = Column(String(200), nullable=False, comment='源地址')
    # 机器检测模式-- 文本检测种类较多，v1版本将分表存储处理。 其格式如: 关键词、智能识别...
    idents_mode = Column(String(50), ChoiceType(VOUCHER_TYPE), nullable=False, comment='机审模式')
    # 识别类型规则-- 如: 涉黄，涉毒，涉政, 反腐等...
    idents_type = Column(String(50), nullable=False, comment='机审类型')
    audit_status = Column(Boolean, server_default=expression.true(), nullable=False, comment='审核状态')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    # 任务、站点、文本存储库-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')
    text_stor_id = Column(Integer, ForeignKey('text_stor.id', ondelete='CASCADE'), comment='文本库关联')
    strategy_id = Column(Integer, ForeignKey('{}.id'.format(Strategy.__tablename__)), comment='策略关联')

    def __str__(self):
        return self.origin_addr
# ---------重构检测结果模型--------
# 现阶段合成一个表，因重复的字段较多，没有拆分关键词和智能检测的结果表
# 只是针对文本类检测和图片类检测才拆分表存储
'''
class KeywordResultStor(BaseModel):
    """
    关键词检测结果存储
    """
    __tablename__ = 'kwres_stor'

    origin_addr = Column(String(200), nullable=False, comment='源地址')
    idents_type = Column(String(50), nullable=False, comment='机审类型')
    audit_status = Column(Boolean, server_default=expression.true(), nullable=False, comment='审核状态')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    update_time = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    # 任务、站点、策略、关键词证据-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')
    strategy_id = Column(Integer, ForeignKey('{}.id'.format(Strategy.__tablename__)), comment='策略关联')
    kw_evidence_id = Column(Integer, ForeignKey('kw_evidence.id', ondelete='CASCADE'), comment='关键词证据关联')

    def __str__(self):
        return self.origin_addr

class AiResultStor(BaseModel):
    """
    智能检测结果存储
    """
    __tablename__ = 'aires_stor'

    origin_addr = Column(String(200), nullable=False, comment='源地址')
    idents_type = Column(String(50), nullable=False, comment='机审类型')
    audit_status = Column(Boolean, server_default=expression.true(), nullable=False, comment='审核状态')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    update_time = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    # 任务、站点、策略、关键词证据-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')
    strategy_id = Column(Integer, ForeignKey('{}.id'.format(Strategy.__tablename__)), comment='策略关联')
    ai_evidence_id = Column(Integer, ForeignKey('ai_evidence.id', ondelete='CASCADE'), comment='智能检测证据关联')

    def __str__(self):
        return self.origin_addr
# ---------重构模型--------
'''

class ImageResultStor(BaseModel):
    """
    图片检测结果存储
    """
    __tablename__ = 'image_res'

    origin_addr = Column(String(200), nullable=False, comment='源地址')
    local_addr = Column(String(200), nullable=False, comment='本地存储位置')
    # 机器检测模式-- 图片检测种类单一， 目前只支持智能检测: type: ai
    idents_mode = Column(String(50), ChoiceType(VOUCHER_TYPE), nullable=False, comment='机审模式')
    # 识别类型-- 如: 现只支持涉黄类型，之后会增加(涉毒，涉政, 反腐等多种检测类型...)
    idents_type = Column(String(50), nullable=False, comment='机审类型')
    audit_status = Column(Boolean, server_default=expression.true(), nullable=False, comment='审核状态')
    score = Column(FLOAT(precision=10, decimal_return_scale=2), nullable=False, comment='图片分值')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    # 任务、站点、文本、策略-关联
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__)), comment='任务关联')
    site_id = Column(Integer, ForeignKey('{}.id'.format(Site.__tablename__)), comment='网站关联')
    strategy_id = Column(Integer, ForeignKey('{}.id'.format(Strategy.__tablename__)), comment='策略关联')
    image_stor_id = Column(Integer, ForeignKey('image_stor.id', ondelete='CASCADE'), comment='图片库关联')

    def __str__(self):
        return self.origin_addr

class KwBody(BaseModel):
    """
    关键词检测证据存储
    """
    __tablename__ = 'kw_evidence'

    # original_text = Column(Text, comment='原始内容')
    # voucher_type = Column(String(25), nullable=False, comment='凭证类型')
    keyword = Column(String(25), nullable=False, comment='关键词')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    update_time = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    # 关键词检测结果/原始文本-关联
    text_res_id = Column(Integer, ForeignKey('text_res.id', ondelete='CASCADE'))
    # text_stor_id = Column(Integer, ForeignKey('text_stor.id', ondelete='CASCADE'))

    def __str__(self):
        return 'keyword body: {}'.format(self.keyword)

class AiBody(BaseModel):
    """
    智能检测文本证据存储
    """
    __tablename__ = 'ai_evidence'

    # original_text = Column(Text, comment='原始内容')
    # voucher_type = Column(String(25), nullable=False, comment='凭证类型')
    # aidetec_type: 只针对智能检测，如：涉黄-涉政...
    aidetec_type = Column(String(25), nullable=False, comment='智能识别类型')
    last_time = Column(DateTime, default=timezone.now, comment='发现时间')
    update_time = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    # 文本检测结果/原始文本-关联
    text_res_id = Column(Integer, ForeignKey('text_res.id', ondelete='CASCADE'))
    # text_stor_id = Column(Integer, ForeignKey('text_stor.id', ondelete='CASCADE'))

    def __str__(self):
        return 'aicheck body: {}'.format(self.aidetec_type)
