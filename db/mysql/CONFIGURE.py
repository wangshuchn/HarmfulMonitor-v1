#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : CONFIGURE.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/18 18:05
    *  @Software : PyCharm
    ***********************************
    配置
"""
from db.mysql import BaseModel
from sqlalchemy import Column
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime)

from db.mysql.RBAC import User
from db.mysql.TASK import Task
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone


class HfBot(BaseModel):
    """
    爬虫配置
    """
    __tablename__ = 'spider_conf'

    max_page = Column(Integer, nullable=True, comment='页面数')
    thread_count = Column(Integer, nullable=True, comment='并发数')
    delay = Column(Integer, nullable=True, comment='延迟')
    depth_limit = Column(Integer, nullable=False, default=0, comment='爬取深度')

    def __str__(self):
        return self.max_page

CONF_MODE = [
    (u'0', u'文本'),
    (u'1', u'图片'),
    (u'1', u'关键词'),
]
class Ai_detec_conf(BaseModel):
    """
    文本、图片检测配置
    """
    __tablename__ = 'task_conf'

    # 说明: 智能检测：文本还是图片
    # 用处: 策略里的类型，在修改任务配置和检测、重新检测的功能上使用
    conf_mode = Column(ChoiceType(CONF_MODE), nullable=False, comment='配置模型')
    # 说明: 需要cherry检测类型：涉黄-涉暴-反腐....
    # 用处: 策略里的类型，在修改任务配置和检测、重新检测的功能上使用
    conf_type = Column(String(220), nullable=True, comment='配置类型')
    # 说明: 创建时间
    # 用处: 可以在UI上展现何时创建
    create_at = Column(DateTime, default=timezone.now, comment='创建时间')
    # 说明: 开始时间
    # 用处: 预留，方便UI上查看检测时间信息
    start_at = Column(DateTime, default=timezone.now, comment='开始时间')
    # 说明: 结束时间
    # 用处: 预留，方便UI上查看检测时间信息
    end_at = Column(DateTime, default=timezone.now, comment='结束时间')
    # 说明: 用户
    # 用处: 为了支持多用户
    user_id = Column(Integer, ForeignKey('{}.id'.format(User.__tablename__), ondelete='CASCADE'))
    # 说明: 任务
    # 用处: 关联是哪个任务配置下的哪次任务
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__), ondelete='CASCADE'))

    def __str__(self):
        return self.conf_mode


#
# FIXME: 此处Model设计存在耦合
'''
class Keyword_detec_conf(BaseModel):
    """
    关键词检测配置
    """
    __tablename__ = 'kwtask_conf'

    # 说明: 需要检测的关键词类型：涉黄-涉暴-反腐....
    # 用处: 策略里的类型，在修改任务配置和检测、重新检测的功能上使用
    conf_type = Column(String(50), nullable=False, comment='配置类型')
    # 说明: 创建时间
    # 用处: 可以在UI上展现何时创建
    create_at = Column(DateTime, default=timezone.now, comment='创建时间')
    # 说明: 开始时间
    # 用处: 预留，方便UI上查看检测时间信息
    start_at = Column(DateTime, default=timezone.now, comment='开始时间')
    # 说明: 结束时间
    # 用处: 预留，方便UI上查看检测时间信息
    end_at = Column(DateTime, default=timezone.now, comment='结束时间')
    # 说明: 用户
    # 用处: 为了支持多用户
    user_id = Column(Integer, ForeignKey('{}.id'.format(User.__tablename__), ondelete='CASCADE'))
    # 说明: 任务
    # 用处: 关联是哪个任务配置下的哪次任务
    task_id = Column(Integer, ForeignKey('{}.id'.format(Task.__tablename__), ondelete='CASCADE'))

    def __str__(self):
        return self.conf_type
'''

# ****************统计类*******************
'''
class Stat(BaseModel):
    """
    统计-
    前台: 展示基本的、常用的统计信息, 用于站点监测的展示
    由分析统计模块写入数据
    """
    __tablename__ = 'stat'
    # 0 - 无；1 - 低；2 - 中； 3 - 高, 哪次监测中，风险等级最高的站点的等级
    level = Column(Integer, nullable=False, default=0, comment='最高站点风险等级')
    high = Column(Integer, nullable=False, default=0, comment='风险等级为高的站点数量')
    medium = Column(Integer, nullable=False, default=0, comment='风险等级为中的站点数量')
    low = Column(Integer, nullable=False, default=0, comment='风险等级为低的站点数量')
    nought = Column(Integer, nullable=False, default=0, comment='风险等级为无的站点数量')
    # 说明: 针对可用性监测
    dead = Column(Integer, nullable=False, default=0, comment='不存活的站点数量')
    # 说明: 针对网站信息监测
    icp = Column(Integer, nullable=False, default=0, comment='ICP备案的站点数量')

    # 说明: 创建时间
    # 用处: 可以在UI上展现何时创建
    create_at = Column(DateTime, default=timezone.now, comment='创建时间')
    # 说明: 开始时间
    # 用处: 预留，方便UI上查看检测时间信息
    start_at = Column(DateTime, default=timezone.now, comment='开始时间')
    # 说明: 结束时间
    # 用处: 预留，方便UI上查看检测时间信息
    end_at = Column(DateTime, default=timezone.now, comment='结束时间')

    def __str__(self):
        return self.create_at
'''