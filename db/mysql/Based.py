#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : Based.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 9:41
    *  @Software : PyCharm
    ***********************************
"""
import time
from uuid import uuid4
from sqlalchemy import String, Integer
from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base

class Model(object):
    """
     ORM模型基类
    """
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8',
        'extend_existing': True,
    }
    @property
    def columns(self):
        return [c.name for c in self.__table__.columns]

    @property
    def columnitems(self):
        return dict([(c, getattr(self, c)) for c in self.columns])

    @property
    def dict(self):
        return {c.name: getattr(self, c.name, None) for c in self.__table__.columns}

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, self.columnitems)

    def tojson(self):
        return self.columnitems


class BaseModelObj(Model):
    """
    基模
    """
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True, comment='默认id')   # 默认生成id

BaseModel = declarative_base(cls=BaseModelObj)