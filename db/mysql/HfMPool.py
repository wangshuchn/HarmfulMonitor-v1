#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : HfMPool.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 10:55
    *  @Software : PyCharm
    ***********************************
"""
from core.utils import Db
from pymysql.err import OperationalError

class ConnectPool(object):
    """
    获取数据库连接池
    // 本地测试
    dict(host='127.0.0.1',
                                                         port=3306,
                                                         user='root',
                                                         password='123456',
                                                         db_name='hfmsec')
    """
    def __init__(self):
        try:
            self.db_pool = Db.HfMysqlPool('HfMonitor')
        except OperationalError:
            self.db_pool = None
        # self.cache_redis = Redis(host=redis_conf['host'], port=redis_conf['port'], db=redis_conf['db'])

    def get_dbPool(self):
        """
        返回连接池对象
        """
        return self.db_pool
