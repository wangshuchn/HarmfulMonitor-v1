#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : INITDB.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 23:00
    *  @Software : PyCharm
    ***********************************
    系统数据库表模型初始化
"""
import os
import sys
import logging
from conf.config import (unitymob, echo, pool_size, pool_recycle, autoflush,
                         SUPER_USER, SUPER_USER_PASSWORD)
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database, drop_database
from db.mysql.RBAC import User
from db.mysql.Based import BaseModel
from core.auth.hashers import make_password

log_path = os.path.join(os.getcwd(), 'logs{0}{1}.log'.format(os.path.sep,'create_db'))

logger = logging.getLogger('HarmfulMIS-v1')
logger.setLevel(logging.DEBUG)

logger.propagate = False
initdb_handler = logging.FileHandler(filename=log_path, encoding='utf-8')
initdb_format = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s%(message)s')
initdb_handler.setFormatter(initdb_format)
logger.addHandler(initdb_handler)

def init_db():
    """
    创建表
    :return:
    """

    _db_engine = create_engine(unitymob,
                               echo=echo,
                               pool_size=pool_size,
                               pool_recycle=pool_recycle,
                               pool_pre_ping=True)

    # db session
    db_session = sessionmaker(bind=_db_engine,
                             autoflush=autoflush)

    # print(_db_engine.connect())
    # 如果sechfm项目库不存在，则创建
    if not database_exists(_db_engine.url):
        create_database(_db_engine.url, encoding='utf8mb4')
        # 生成表
        BaseModel.metadata.create_all(_db_engine)
        if db_session:
            session = db_session()
            init_super_user = User(
                username=SUPER_USER,
                password=make_password(SUPER_USER_PASSWORD),
                realname='系统管理员',
                job_title='系统管理员',
                is_superuser=True,
                is_active=True,
                telphone='18701015792',
                email='sechfm@cecgw.cn',
                description='系统管理员，拥有至尊无上的权利',
            )
            session.add(init_super_user)
            session.commit()
            session.close()
        print("【温馨提示】已成功生成数据库【sechfm】初始化系统管理员【{user}】".format(user=SUPER_USER))
        logger.info('【恭喜】已成功生成数据库【sechfm】初始化系统管理员【{user}】'.format(user=SUPER_USER))
    else:
        print('[*]Check Server running normally[Succeed]')
        logger.info('检查服务器是否正常运行【成功】')

    return db_session


def drop_db():
    """
    【*】为方便初期测试阶段使用，慎用此操作！！!
    根据类删除数据库表
    :return:
    """
    keyup = input("【敏感操作，请及时备份数据信息!】您确定要删除项目库<sechfm>吗? 确认删除后则无法恢复!【yes/no】\n")
    if keyup == 'yes':
        print("*" * 49)
        print("*" * 16, '正在删除数据库...', "*" * 16)
        _db_engine = create_engine(unitymob,
                                   echo=echo,
                                   pool_size=pool_size,
                                   pool_recycle=pool_recycle)
        # 【慎用】如果sechfm项目库存在，则删除
        if database_exists(_db_engine.url):
            drop_database(_db_engine.url)
            print("【温馨提示】已成功删除数据库以及数据表")
            logger.info('已成功删除数据库【sechfm】并删除项目表')
        else:
            print('【警告】数据库不存在，无法删除')
            print("*" * 49)
            logger.info('【警告】数据库【sechfm】不存在，无法删除')
    else:
        print("*" * 16, '退出操作', "*" * 16)
        sys.exit(1)


if __name__ == '__main__':
    init_db()
    # drop_db()