#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : SYSLOG.py
    *  @Author : wshu
    *  @CodeDate : 2020/5/20 15:53
    *  @Software : PyCharm
    ***********************************
    日志管理
"""
from . import BaseModel
from sqlalchemy import Column
from sqlalchemy.sql import expression
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime)

from .RBAC import User
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone

class SystemLog(BaseModel):
    type = Column(String(50), comment='操作类型')
    user = ForeignKey(User, ondelete='CASCADE')
    action = Column(Text, comment='用户操作')
    status = Column(Boolean, default=expression.true, comment='操作状态')
    create_at = Column(DateTime, server_default=timezone.now, comment='操作时间')

    def __str__(self):
        return self.type