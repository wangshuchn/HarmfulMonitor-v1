-- #!/usr/bin/env python
-- # -*- coding:utf-8 -*-
-- __author__ = 'wshu'
-- __version__ = '1.0'
-- """
--     ***********************************
--     *  @CodeDate : 2020/4/15 16:09
--     *  @Software : PyCharm
--     ***********************************
--     项目MYSQL表结构
-- """
-- # ---users---
-- """
--     CREATE TABLE user(
--     id int not null AUTO_INCREMENT,
--     username varchar(50) not null unique,
--     password varchar(128) not null,
--     is_superuser tinyint(1) not null,
--     is_staff tinyint(1) not null,
--     is_active tinyint(1) not null,
--     date_joined datetime(6) not null,
--     job_title varchar(50) not null,
--     telphone varchar(18),
--     email varchar(50),
--     description longtext not null,
--     error_count int(11) not null,
--     lock_time int(11) not null,
--     primary key(id)
--     )
-- """
-- # ---tasks---
-- # 任务
-- """
--     CREATE TABLE task(
--     id int not null AUTO_INCREMENT,
--     task_id varchar(50) not null unique,
--     task_name varchar(30) not null,
--     task_info longtext,
--     task_status varchar(25) not null default '',
--     task_plan_time Datetime(6),
--     task_plan_end_time datetime(6),
--     task_starttime datetime(6),
--     task_endtime datetime(6),
--     primary key(id),
--     constraint userid foreign key(id) references user(id) on delete cascade on update cascade
--     )
-- """
-- # 站点
-- """
--     CREATE TABLE site(
--     id int not null AUTO_INCREMENT,
--     site_name varchar(200) not null,
--     site_status varchar(25) not null,
--     task_starttime datetime(6),
--     task_endtime datetime(6),
--     primary key(id),
--     constraint userid foreign key(id) references user(id) on delete cascade on update cascade
--     )
-- """
-- # ---spider---
-- # 文本
-- """
--     CREATE TABLE textsta(
--     id int not null AUTO_INCREMENT,
--     url varchar(220) not null,
--     body longtext not null,
--     detect_status varchar(25) not null,
--     grab_time datetime(6) not null,
--     primary key(id),
--     )
-- """
-- # 图片
-- pictureta = """
--     CREATE TABLE pictureta(
--     id int not null AUTO_INCREMENT,
--     imgurl varchar(220) not null,
--     imgpath varchar(200) not null,
--     imgsize varchar(50),
--     detect_status varchar(25) not null default 0,
--     grab_time datetime(6) not null,
--     primary key(id),
--     )
-- """
-- # 结果
-- """
--     CREATE TABLE detect_result(
--     )
-- """
