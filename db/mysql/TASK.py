#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : TASK.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 12:06
    *  @Software : PyCharm
    ***********************************
"""
from . import BaseModel
from sqlalchemy import Column
from sqlalchemy.sql import expression
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime)

from .RBAC import User
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone

# 安全事件检测状态
SAFE_EVENT_STATUS = [
    (u'false', u'关闭'),
    (u'true', u'开启'),
]
# 任务周期
TASK_PLAN = [
    (u'单次', u'单次'),
    (u'日', u'日'),
    (u'周', u'周'),
    (u'月', u'月'),
    (u'季', u'季'),
]
# 任务状态
TASK_STATUS = [
    (u'0', u'待执行'),
    (u'1', u'执行中'),
    (u'2', u'已完成'),
    (u'3', u'已结束'),
]

# 任务类型
TASK_TYPE = [
    (u'0', u'资产发现'),
    (u'1', u'内容监测'),
    (u'2', u'安全巡检'),
]
#

class Task(BaseModel):
    """
    任务模型
    """
    __tablename__ = 'task'

    task_id = Column(String(50), nullable=False, unique=True, comment='任务编号')    # 任务编号
    task_name = Column(String(30), nullable=False, comment='任务名称')   # 任务名称
    task_plan = Column(String(25), ChoiceType(TASK_PLAN), comment='任务周期')       # 任务周期
    distort = Column(Boolean, server_default=expression.true(), nullable=False, comment='篡改')      # 篡改
    darklink = Column(Boolean, server_default=expression.true(), nullable=False, comment='暗链')     # 暗链
    hanghorse = Column(Boolean, server_default=expression.true(), nullable=False, comment='挂马')    # 挂马
    usable = Column(Boolean, server_default=expression.true(), nullable=False, comment='可用性')      # 可用性
    task_type = Column(String(25), ChoiceType(TASK_TYPE), comment='任务类型')       # 任务类型
    task_info = Column(Text, comment='任务描述')   # 任务描述
    task_status = Column(String(25), ChoiceType(TASK_STATUS), default='0', comment='任务状态')        # 任务状态
    plan_run_at = Column(DateTime, nullable=True, comment='计划执行时间')    # 计划执行时间
    plan_stop_at = Column(DateTime, nullable=True, comment='计划结束时间')  # 计划结束时间
    create_at = Column(DateTime, default=timezone.now, comment='创建时间')   # 任务创建时间
    start_at = Column(DateTime, default=timezone.now, nullable=True, comment='开始时间')   # 任务开始时间
    end_at = Column(DateTime, default=timezone.now, nullable=True, onupdate=timezone.now, comment='结束时间')
    # 用户-关联
    user_id = Column(Integer, ForeignKey('{}.id'.format(User.__tablename__), ondelete='CASCADE'))
    task_site = relationship('Site',
                             backref='task_for_site',
                             lazy='dynamic',
                             cascade='all, delete-orphan',
                             passive_deletes=True)
    def __str__(self):
        return "{task_id}--{task_name}".format(task_id=self.task_id, task_name=self.task_name)


SITE_STATUS = [
    (u'0', u'待抓取'),
    (u'1', u'以抓取'),
]

class Site(BaseModel):
    """
    站点模型
    """
    __tablename__ = 'site'

    site_name = Column(String(200), nullable=False, comment='站点域名')
    site_ip = Column(String(50), nullable=True, default='0.0.0.0', comment='站点IP')
    status = Column(String(25), ChoiceType(SITE_STATUS), default='1', comment='是否抓取')
    start_at = Column(DateTime, default=timezone.now, comment='开始时间')
    end_at = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='结束时间')
    # 任务/用户关联
    task_id = Column(Integer, ForeignKey('task.id', ondelete='CASCADE'))
    user_id = Column(Integer, ForeignKey('{}.id'.format(User.__tablename__), ondelete='CASCADE'))

    def __str__(self):
        return self.site_name