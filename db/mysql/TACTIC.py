#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : TACTIC.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 14:31
    *  @Software : PyCharm
    ***********************************
    策略模型
"""

from . import BaseModel
from sqlalchemy import Column
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy import PrimaryKeyConstraint, UniqueConstraint, CheckConstraint
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime, LargeBinary)

from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone


class Strategy(BaseModel):
    """
    策略模型
    """
    __tablename__ = 'strategy'

    strategy_id = Column(String(50), nullable=False, unique=True, comment='策略编号')
    strategy_name = Column(String(30), unique=True, comment='策略名称')
    describe = Column(Text, nullable=True, comment='策略描述')
    starttime = Column(DateTime, default=timezone.now, comment='添加时间')
    updatetime = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')

    def __str__(self):
        return self.strategy_name


class KeywordStrategy(BaseModel):
    """
    关键词策略模型
    """
    __tablename__ = 'keyword_strategy'

    keyword = Column(String(50), nullable=False, comment='关键词名称')
    upadtetime = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    strategy_id = Column(String(30), ForeignKey('strategy.strategy_id', ondelete='CASCADE'), comment='策略')

    def __str__(self):
        return self.keyword

class TextStrategy(BaseModel):
    """
    智能检测文字识别
    """
    __tablename__ = 'text_strategy'

    content = Column(Text, nullable=False, comment='文本输入')
    upadtetime = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    strategy_id = Column(String(30), ForeignKey('strategy.strategy_id', ondelete='CASCADE'), comment='策略')

    def __str__(self):
        return self.content

class ImageStrategy(BaseModel):
    """
    智能检测图片识别
    """
    __tablename__ = 'image_strategy'

    imgbinary = Column(LargeBinary, nullable=False, comment='图片')
    upadtetime = Column(DateTime, default=timezone.now, onupdate=timezone.now, comment='更新时间')
    strategy_id = Column(String(30), ForeignKey('strategy.strategy_id', ondelete='CASCADE'), comment='策略')

    def __str__(self):
        return self.imgbinary