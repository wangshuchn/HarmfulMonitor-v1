#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : __init__.py.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/25 14:47
    *  @Software : PyCharm
    ***********************************
"""
from .Based import BaseModel
from .RBAC import (User, Role,
                   Menu, Permission)

from .TASK import (Task, Site)
from .TACTIC import (Strategy, KeywordStrategy,
                     TextStrategy, ImageStrategy)

from .MEDIASTOR import (TextStor, ImageStor, BadurlStor, TextResultStor, ImageResultStor,
                        KwBody, AiBody)

from .NOTICE import Notice

from .CONFIGURE import (# Keyword_detec_conf,
                        Ai_detec_conf,
                        HfBot)

import pymysql

__all__ = [BaseModel,
           User, Role, Menu, Permission,
           Task, Site,
           Strategy, KeywordStrategy, TextStrategy, ImageStrategy,
           TextStor, ImageStor, BadurlStor, TextResultStor, ImageResultStor, KwBody, AiBody,
           Ai_detec_conf,
           Notice,
           ]
# Keyword_detec_conf,

pymysql.install_as_MySQLdb()