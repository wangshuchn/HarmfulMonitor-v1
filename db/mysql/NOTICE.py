#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : NOTICE.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/16 13:26
    *  @Software : PyCharm
    ***********************************
"""
from . import BaseModel
from sqlalchemy import Column
from sqlalchemy import ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import (String, Integer, Boolean, Text, DateTime)

from .RBAC import User
from sqlalchemy_utils.types.choice import ChoiceType
from core.comm.utils import timezone


# 检测事件状态
NOTICE_TYPE = [
    (u'asset', u'资产发现'),
    (u'content', u'内容监测'),
    (u'safeevent', u'安全事件'),
]

class Notice(BaseModel):
    """
    通知模型
    """
    __tablename__ = 'notice'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
    notice_title = Column(String(50), comment='通知标题')
    notice_body = Column(Text, comment='通知内容')
    notice_status = Column(Boolean(0), comment='阅读状态', default='0')
    notice_url = Column(String(50), comment='父链接')
    notice_type = Column(String(30), ChoiceType(NOTICE_TYPE), comment='通知类型')
    notice_time = Column(DateTime, default=timezone.now, comment='通知日期')
    # 用户关联
    user_id = Column(Integer, ForeignKey('{}.id'.format(User.__tablename__)), comment='所属用户')

    def __str__(self):
        return self.notice_title