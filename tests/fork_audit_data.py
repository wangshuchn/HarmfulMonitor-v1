#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : fork_audit_data.py
    *  @Author : wshu
    *  @CodeDate : 2020/6/18 17:08
    *  @Software : PyCharm
    ***********************************
"""
import pymysql.cursors

# 数据存储
text_url = 'http://exceptcion.com/asp?id=1'
text_body = 'Chinaplas 2018 中国国际橡塑展 2018 C'
snapshot = ''
grab_time = '2020-06-18 16:11:23'
task_id = 26
site_id = 2759

# 检测存储
origin_addr = 'http://exceptcion.com/asp?id=1'
idents_mode = '关键词'
idents_type = '涉黄'
audit_status = 2
taskid = 26
siteid = 2759
text_stor_id = 2
strate_id = 47

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='Ws123456,mysql',
                             db='sechfm',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

# with connection.cursor() as cursor:
#     sql = "insert into text_stor (text_url, text_body, snapshot, task_id, site_id) values (%s,%s,%s,%s,%s)"
#     cursor.execute(sql, (text_url, text_body, snapshot, task_id, site_id))
#     connection.commit()


with connection.cursor() as cursor:
    sql = "insert into text_res (origin_addr, idents_mode, idents_type, audit_status, task_id, site_id, text_stor_id, strategy_id) values (%s,%s,%s,%s,%s,%s,%s,%s)"
    cursor.execute(sql, (origin_addr, idents_mode, idents_type, audit_status, taskid, siteid, text_stor_id, strate_id))
    connection.commit()

