#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
"""
    ***********************************
    *  @filename : proxyi.py
    *  @Author : wshu
    *  @CodeDate : 2020/3/9 10:36
    *  @Software : PyCharm
    ***********************************
"""
import requests

proxies = {
  'http': 'http://xx.xx.xx.xx:xx'
}
rsp = requests.get("http://www.baidu.com", proxies=proxies)
print(rsp.status_code)
