#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : pickle_test.py
    *  @Author : wshu
    *  @CodeDate : 2020/7/3 10:08
    *  @Software : PyCharm
    ***********************************
"""
import pickle

def pick_load_data(bytes_str):
    try:
        assert bytes_str is not None
        return pickle.loads(bytes_str)
    except AssertionError:
        return None

def pick_dump_data(data_str):
    return pickle.dumps(data_str)


if __name__ == '__main__':
    pick_string = list(range(10))
    pick_res = pick_dump_data(pick_string)
    print(pick_res)
    source_res = pick_load_data(pick_res)
    print(source_res)