#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : url.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 21:03
    *  @Software : PyCharm
    ***********************************
"""
from io import StringIO
from urllib.parse import urlparse
import os, imghdr
try:
    from pillow import Image
except ImportError:
    from PIL import Image

from deface.conf.consts import *
from deface.conf import settings

def _get_url_ext(url):
    '''
    获取url扩展名
    '''
    parsed = urlparse(url)
    root, ext = os.path.splitext(parsed.path)
    if ext:
        ext = ext[1:].lower()
    return ext


def is_text(item):
    '''
    是否纯文本
    '''
    # 优先content_type
    c = item.get('content_type')
    if c:
        if c.startswith('text') or c in TEXT_MIMETYPES:
            return True
        return False

    # 然后url ext判断
    ext = _get_url_ext(item.url)
    if ext and ext in TEXT_EXTS:
        return True
    return False


def is_image(item):
    '''
    轻量的图片url检测，可能不准确
    如果使用了缓存，服务器响应304的话会没有content-type一项
    '''
    c = item.get('content_type')
    if c:
        return c.startswith('image/')

    if item.body:
        return imghdr.what('', item.body)

    ext = _get_url_ext(item.url)
    if ext and ext in IMAGE_EXTS:
        return True
    return False


def is_authcode(item, headers):
    '''
    是否是验证码url
    '''
    # 首先必须是图片
    if not is_image(item):
        return False

    # 无缓存的一般都是
    if headers.get('pragma') == 'no-cache' or \
            headers.get('cache-control') == 'no-store' or \
            headers.get('cache-control') == 'no-cache':
        return True

    # 黑名单
    for i in AUTHCODE_BLACKLIST:
        if i in item.url:
            return True
    return False


def get_image_resolution(img):
    '''
    获取图片分辨率, 失败返回False
    '''
    try:
        size = Image.open(StringIO(img)).size
        return '%sx%s' % (size[0], size[1])
    except (ImportError, IOError):
        # 不支持的图片格式，不处理
        return False
    except:
        logger.exception('image deal error')
        return False


def in_content_whitelist(item):
    '''
    判断html内容是否含白名单关键词
    '''
    html = item.body
    if html:
        for i in CONTENT_WHITE_LIST:
            if html.find(i) != -1:
                return True
    return False


def in_url_whitelist(item):
    '''
    判断url是否在白名单里
    '''
    url = item.url
    for i in URL_WHITE_LIST:
        if url.find(i) != -1:
            return True
    return False


def get_hacked_keyword(item):
    '''
    检查被黑关键词
    '''
    html = item.body
    if html:
        for i in settings.HACKED_KEYWORDS:
            for j in settings.EXCLUDE_HACKED_KEYWORDS:
                # 如果同时出现就认为可以排除，但有漏报可能
                try:
                    if html.find(i) != -1 and html.find(j) == -1:
                        return i
                except UnicodeDecodeError:
                    print('html: %s, i: %s, j: %s' % (html, i, j))


def trim_content_type(ct):
    '''
    去掉charset字串
    '''
    if ct:
        t = ct.find(';')
        if t != -1:
            return ct[:t].lower()
    return ct.lower()