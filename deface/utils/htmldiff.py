#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : htmldiff.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 21:01
    *  @Software : PyCharm
    ***********************************
"""
from difflib import HtmlDiff as _HtmlDiff

class HtmlDiff(_HtmlDiff):
	"""
	用css class区分了原始和被篡改部分
	"""
	def _format_line(self,side,flag,linenum,text):
		try:
			linenum = '%d' % linenum
			id = ' id="%s%s"' % (self._prefix[side],linenum)
		except TypeError:
			id = ''
		text=text.replace("&","&amp;").replace(">","&gt;").replace("<","&lt;")
		text = text.replace(' ','&nbsp;').rstrip()
		td_class = 'td-from' if side == 0 else 'td-to'

		return '<td class="diff_header"%s>%s</td><td class="%s" nowrap="nowrap">%s</td>' % (id, linenum, td_class, text)