#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : settings.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 21:12
    *  @Software : PyCharm
    ***********************************
"""
# 读取全局配置
from conf import config

# 爬虫封装
SPIDER_LOADER = 'HarmfulMIS-v1-dev.core.deface.spider.loader'

# 存储封装
STORAGE = 'HarmfulMIS-v1-dev.core.deface.storage.mongodb.MongodbStorage'
STORAGE_SETTINGS = {
	'db': 'websoc',
	'event_table': 'websoc_event',  # 存event的表, websoc要用
}
REDIS_IP = config.redis_ip
REDIS_PORT = config.redis_port
PROGRESS_ENABLE = False

SPIDER_DEBUG = False  # 日志
STATS_ENABLE = False  # 开启统计
EVIDENCE_ENABLE = False  # 开启取证

### websoc 相关概念 ###
_SITE_ID = 0
_TASK_ID = 0
_GROUP_ID = 0
_G_SITE_ID = None
_V_GROUP_ID = None
_USER_ID = 0
_SITE_MD5 = ''
#######################

# 任务号，0为临时任务
TASK_ID = 0
# 基准任务号，-1表示不比较，仅创建基准
BASE_ID = -1
# 待检测站点
SITE = 'http://news.baidu.com/'
# 忽略验证码url
IGNORE_AUTHCODE = False
# 模式：由以下数字异或得来：1-检测纯文本网页 2-检测图片 4-检测其他二进制文件
MODE = 7
# 图片比较方式：1-分辨率比较，2-文件大小比较，3-分辨率和文件大小都比较, 4-默认(diff对比)，
IMAGE_OPTION = 0
# 纯文本网页检测模式：1-网页框架比较 2-网页内容比较 3网页框架和内容都比较
TEXT_OPTION = 3
# 爬虫深度
DEPTH = 3
# 报警级别，1～4，轻微变更、中度变更、高度变更、确认篡改
CHECK_LEVEL = 3
# 爬虫线程数
NUM_THREADS = 20
# URL数量限制
MAX_URLS = 100
# 页面抓取超时
FETCH_TIMEOUT = 20
# 页面抓取间隔
FETCH_DELAY = 0
# 页面大小上限。超过这个上限的页面不抓取
MAX_FETCH_SIZE = 1048576 # 1MB
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.65 Safari/534.24'
# http 代理，格式：http://url:port
HTTP_PROXY = ''
# 预设cookies
COOKIES = ''
# http 认证，格式：('basic', 'user', 'pass')，可选类型：basic, digest, ntlm
HTTP_AUTH = ()
# 心跳时间（秒），超过这个时间段未响应会被外部程序 kill 掉。默认 10 分钟
HEART_BEAT_INTERVAL = 600

# 判断篡改等级时用到的阀值, 必须在(0, 1)间
# 0----(A)----THRESHOLD_MID-----(B)-----THRESHOLD_LOW----(C)----THRESHOLD_NONE----(D)---1----(D)--
# ratio 落在 A 区间表示：高度篡改
#			 B 区间表示：中度篡改
#			 C 区间表示：轻度篡改
#			 D 区间表示：未篡改
# ratio 分框架和内容的：一个 B 和 B 视为 A
#						一个 A 和 B 视为 A
#						一个 A 和 A 视为 A
#						一个 A 和 C 视为 B
#						一个 C 和 C 视为 B
#						一个 B 和 C 视为 B
#						一个 A 和 D 视为 B
#						一个 B 和 D 视为 C
#						一个 C 和 D 视为 C
#						一个 D 和 D 视为 D
# 如果最终 ratio 为 A 并且文本含有被黑关键字，视为 确认篡改
# 未篡改。越小越容易漏报，越大越容易误报
THRESHOLD_NONE = 0.998870
# 轻度篡改
THRESHOLD_LOW = 0.9884285
# 中度篡改
THRESHOLD_MID = 0.608143
#socket read() 超时时间
SOCKET_READ_TIMEOUT = 10
#单次请求的最大数据大小，默认1M
SOCKET_MAX_READSIZE = 1000000

# 被黑关键词
HACKED_KEYWORDS = [
	u'友情检测', u'您的网站存在严重漏洞', u'黑客联盟', u'贵站存在安全隐患', u'贵站有漏洞', 'hacked by',
	'hack by', u'被挂黑页', u'本站已被入侵', u'请及时修补', u'请管理员修补', u'此站被黑',
	u'此站以被黑', u'此站已被黑', u'贵站存在严重漏洞', 'hacking', 'Iranian Cyber Army', 'HackeD By EW',
	'Hacked By-NeFReT', 'HacKeD by hacker_20', 'Hacked By H4ckx7', 'QQ-> 23026583', 'HACKED BY ONELAKE',
	'Hacking attempt', 'Hacked by FYT', 'Hacked by:liner', u'联系QQ46071189',
	'www.cybersystemteam.com', 'Hacked by ulow',
]

# 排除的被黑关键词
EXCLUDE_HACKED_KEYWORDS = [
	'hack by Dean Edwards',  # http://code.jquery.com/jquery-1.10.2.js
]

# 被黑页面URL
# FIXME 不能纳入计数
HACKED_URLS = []

# 被黑页面
HACKED_PAGES = [
	'1.asp', '1.html', '1.txt', '11.jsp', '110.htm', '1111.txt', '123.asp',
	'1937cN.html', '2.txt', '2013-1-31115600.htm', '2013021521226095.png',
	'201323152729847.asp', '2b.asp', '404.txt', '520.asp', '9.html', 'BX.txt',
	'Learner.htm', 'M4sk.asp', 'M4sk.html', 'M4sk.txt', 'T0P.asp',
	'User_ReceiveArticlList.asp', 'Vc.asp', 'bingke.asp', 'caobi.txt',
	'dhthacker.com.htm', 'ff0000.html', 'fs.htm', 'hack.htm', 'hack.html',
	'hacked.html', 'hc.html', 'honker.htm', 'mjj.asp', 'qianlan.asp'
	'qq879684922.txt', 'r00t.htm', 's.htm', 'sbhack.html', 'sck.html',
	'shell.asp', 'top.asp', 'x.html', 'xh.txt', 'xiangsi.asp', 'xiaoyao.txt',
	'xiaozi.asp', 'yihen.txt', 'yijuhua.asp', 'zhainan.asp', 'zhainan.html',
]
for i in ['hack', 'hacked', 'fuck', 'fucked', 'x', 'xx', 'xxx'] + [
	str(i) for i in range(10)
]:
	for j in ['txt', 'htm', 'html', 'asp', 'jsp', 'php', 'do', 'xml']:
		HACKED_PAGES.append('%s.%s' % (i, j))
HACKED_PAGES = list(set(HACKED_PAGES))