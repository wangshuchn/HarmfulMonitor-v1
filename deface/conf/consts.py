#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : consts.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 21:11
    *  @Software : PyCharm
    ***********************************
"""
# 篡改类型：增、删、改
DEFACE_NEW = 0
DEFACE_DEL = 1
DEFACE_MOD = 2

# 告警级别
# 新增
LEVEL_NEW = 2
# 删除
LEVEL_DEL = 3
# 二进制
LEVEL_BIN = 3
# 图片
LEVEL_IMG = 3
# 文件类型改变
LEVEL_TYPE = 3

# 等级
# 被篡改
LEVEL_HACKED = 4

# 被黑类型
HACKED_TYPE_KEYWORD = 1  # 被黑关键词
HACKED_TYPE_PAGE = 2  # 被黑页面

# 文本content-type, 不包含以 "text/" 开头的类型
TEXT_MIMETYPES = [
	'message/rfc822',
	'application/postscript',
	'application/javascript',
	'application/x-javascript',
	'application/x-sh',
	'application/x-csh',
	'application/xml',
	'application/rss+xml',
	'application/atom+xml',
	'application/hta',
	'application/sgml',
	'application/xhtml+xml',
	'application/vnd.google-earth.kml+xml',
	'application/vnd.google-earth.kmz',
	'application/vnd.mozilla.xul+xml',
]
# 图片扩展名
IMAGE_EXTS = ('bmp', 'jpg', 'jpeg', 'png', 'gif', 'ico')

# 文本扩展名
TEXT_EXTS = ('html', 'htm', 'php', 'asp', 'jsp', 'shtml', 'js', 'aspx', 'css')

# 各建站系统的验证码url关键字：phpwind, dedecms, discuz，百度空间，各个微博
AUTHCODE_BLACKLIST = (
	'captcha', 'vdimgck.php', 'GetCode', '/ck.php', 'seccode', 'getimage', 'validateimg.php', 'verifypic',
	'pincode', 'get_image', 'ValidCode', 'validatecode.php', 'vcode', 'verify', 'stat.htm', 'webimg.51.la',
	'authcode', 'CheckImage.do', 'VerifyCodeServlet',
)

# 内容白名单：html里有这些内容都会跳过检测
CONTENT_WHITE_LIST = [
	'<TITLE>域名访问提示</TITLE>', '连接数据库失败，可能数据库密码不对或数据库服务器出错',
	'<b>Discuz! info</b>: Can not connect to MySQL server',
]
# URL白名单：url中含有下列元素之一的会跳过检测
URL_WHITE_LIST = set([])

# in_whitelist 是在编码转换前调用的，html还是str类型，需要兼容gbk类型编码
a = set(CONTENT_WHITE_LIST)
for i in CONTENT_WHITE_LIST:
	a.add(i.decode('utf-8').encode('gbk'))
CONTENT_WHITE_LIST = a
del a