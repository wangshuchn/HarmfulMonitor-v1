#!/usr/bin/env python
# -*- coding:utf-8 -*-
__author__ = 'wshu'
__version__ = '1.0'
"""
    ***********************************
    *  @filename : test_croe.py
    *  @Author : wshu
    *  @CodeDate : 2020/4/15 21:23
    *  @Software : PyCharm
    ***********************************
"""
# 篡改，挂马-检测测试
"""
class CheckTestCase(TestCase):

	def setUp(self):
		super(CheckTestCase, self).setUp()
		settings.STORAGE_SETTINGS['db'] = self.dbname

		core.progress = lambda *args, **kwargs: None  # 空处理
		core.G['url_count'] = 0
		core.G['total_url'] = 1

		pattern = os.path.abspath(os.path.join(
			os.path.dirname(__file__), 'blacklink_examples/*_defaced.html'
		))
		filelist = glob(pattern)
		self.items = []
		for filename in filelist:
			item = AttrDict()
			item.headers = None
			item.url = filename
			item.url_id = md5(filename).hexdigest()
			item.content_type = 'text/html'
			item.status = 200
			with open(filename) as f:
				item.body = f.read()
			self.items.append(item)

	def test_check(self):
		for item in self.items:
			before_count = DefaceBase.collection.count()
			core.check(item)
			after_count = DefaceBase.collection.count()
			self.assertEqual(before_count + 1, after_count)
		# TODO 被黑关键词、被黑页面、变更

	def test_check_if_has_hacked_keyword_and_first_check(self):
		before_count = DefaceEvent.collection.count()
		settings.BASE_ID = 9527
		item = AttrDict()
		item.headers = None
		item.url = 'http://www.example.com/hack.txt'
		item.url_id = md5(item.url).hexdigest()
		item.content_type = 'text/html'
		item.status = 200
		item.body = 'hacked by'
		core.check(item)
		after_count = DefaceEvent.collection.count()
		self.assertEqual(before_count + 1, after_count)

	def test_check_if_has_hacked_keyword_and_not_first_check(self):
		before_event_count = DefaceEvent.collection.count()
		before_evidence_count = Evidence.collection.count()
		settings.BASE_ID = 9527
		item = AttrDict()
		item.headers = None
		item.url = 'http://www.example.com/hack.txt'
		item.url_id = md5(item.url).hexdigest()
		item.content_type = 'text/html'
		item.status = 200
		item.body = 'hacked by'
		core.check(item)
		after_event_count = DefaceEvent.collection.count()
		after_evidence_count = Evidence.collection.count()
		self.assertEqual(before_event_count + 1, after_event_count)
		self.assertEqual(before_evidence_count + 1, after_evidence_count)


class DiffLevelTestCase(TestCase):

	def setUp(self):
		super(DiffLevelTestCase, self).setUp()
		settings.STORAGE_SETTINGS['db'] = self.dbname
		pattern = os.path.abspath(os.path.join(
			os.path.dirname(__file__), 'blacklink_examples/*_defaced.html'
		))
		filelist = glob(pattern)
		self.items = []
		for filename in filelist:
			item = AttrDict()
			item.headers = None
			item.url = filename
			item.content_type = 'text/html'
			with open(filename) as f:
				item.body = f.read()
				try:
					item = core._unicode_item(item)
				except Exception, e:
					self.fail('unicode error with %s: %s' % (item.url, e))
			self.items.append(item)

	def test_diff_level(self):
		for item in self.items:
			filename = item.url.replace('defaced', 'origin')
			base = AttrDict()
			base.url = filename
			base.content_type = 'text/html'
			with open(filename) as f:
				base.body = f.read()
			base = core._unicode_item(base)
			self.assertTrue(core._diff_level(base, item) > 0)
"""